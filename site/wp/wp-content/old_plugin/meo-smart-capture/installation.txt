1. Install and activate the latest versions of:
   - CF7 plugin (http://wordpress.org/plugins/contact-form-7/) - required
   - ACF plugin (https://wordpress.org/plugins/advanced-custom-fields/) - required
   - ACF Options Page plugin (http://www.advancedcustomfields.com/add-ons/options-page/) - required
   - Contact Form DB plugin (http://wordpress.org/plugins/contact-form-7-to-database-extension/) - required
   - Listo plugin (http://wordpress.org/plugins/listo/) - optional (for lists of eg countries)
   - Easy Fancybox (http://wordpress.org/plugins/easy-fancybox/)

2. Install and activate the MEO Smart Capture Plugin

3. Add ACF definitions:
	Name: PDF email details
	Fields:
		1.	Title: email sender
			Name: email_sender
			Type: mail
		2.	Title: email sender name
			Name: email_sender_name
			Type: text
			Formatting: No formatting
		3.	Title: logo
			Name: logo
			Type: Image
		4.	Title: email bcc
			Name: email_bcc
			Type: mail
	Assign to:
		Options page equals Options

	Name: PDF email content
		1.	Title: email subject
			Name: email_subject
			Type: text
			Instructions: Replaceable fields:
				[download_url]
				[lot_code]
				[logo]
				[sender_email]
			Formatting: No formatting
		2.	Title: email content
			Name: email_content
			Type: WYSIWYG editor
			Instructions: Replaceable fields:
				[download_url]
				[lot_code]
				[logo]
				[sender_email]
			Toolbar: Full
	Assign to:
		Page template equals MSC Attachment Request OR
		Taxonomy Term equals lot type (if using MEO real estate development plugin) OR
		Attachment equals All


3. Create a new form in CF7 (one per language), eg
	Form:
		<p><label for="surname">Nom*<br /></label>[text* surname placeholder "Nom*"]</p>
		<p><label for="first_name">Prénom*<br /></label>[text* first_name placeholder "Prénom*"]</p>
		<p><label for="email">Email*<br /></label>[email* email placeholder "Email*"]</p>
		<p><label for="phone">Tél*<br /></label>[text* phone placeholder "Tél*"]</p>
		<p><label for="address">Adresse*<br /></label>[text* address placeholder "Adresse*"]</p>
		<p><label for="postcode">NPA*<br /></label>[text* postcode placeholder "NPA*"]</p>
		<p><label for="city">Lieu*<br /></label>[text* city placeholder "Lieu*"]</p>
		<p><label for="country">Pays*<br /></label>[select* country data:countries.olympic]</p>
		[file_id* file_id]<input type="hidden" name="pdf-download" value="1">* champs obligatoires<br>
		[response]<br><br>
		<p>[submit "Envoyer"]</p>
		<div class="clear"></div>
		<input type="hidden" name="language" value="fr">

	From: [surname], [first_name] <[email]>

	Subject: Development :: PDF commandé

	Body:
		Nom: [surname]
		Prénom: [first_name]
		email: [email]
		Télephone: [phone]
		Adresse: [address]
		NPA: [postcode]
		Lieu: [city]
		Pays: [country]
		Langue: fr

		Item: [lot_code]

4. Create a page (called eg "File request")
	Content: Only the shortcode for the contact form
	Template: MSC Attachment Request
	Complete the "PDF Download" section

5. Create a page (called eg "File Download")
	Content: None
	Template: MSC Attachment Download Handler

6. For lightboxes:
	Install and activate Easy FancyBox plugin
	In Settings .. Media, under FancyBox .. Media, select "iframes" (only) and save
	Then set width and height (for pixels, no unit; for percentage xx% format)
	Ensure any links have a class of fancybox-iframe.  The dimensions can be overriden on individual links with (eg) class="fancybox-iframe {width:600,height:500}"


