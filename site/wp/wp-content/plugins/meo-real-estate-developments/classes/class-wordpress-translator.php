<?php
require_once(dirname(__FILE__) . '/../interfaces/interface-translator.php');

class WordPressTranslator implements Translator {
	public function translate($phrase, $language = null) {
		// TODO: allow explicit language
		return __($phrase, MRED_TEXT_DOMAIN);
	}
}
