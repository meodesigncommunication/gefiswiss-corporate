<?php
interface Translator {
	public function translate($phrase, $language = null);
}
