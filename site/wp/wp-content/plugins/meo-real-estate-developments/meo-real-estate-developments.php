<?php
/*
Plugin Name: MEO Real Estate Developments
Version: 1.1.0
Author: MEO
Author URI: http://www.meo-realestate.com/
*/

/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@meomeo.ch

*/

/*
Version history:
1.0 Initial version
1.1 Added sector between development and building
*/

define('MRED_TEXT_DOMAIN', 'meo-real-estate-developments');
define('MRED_MIN_PHP_VERSION', '5.0');

$required_plugins = array('posts-to-posts/posts-to-posts.php');

function mred_showversionnotice() {
	echo '<div class="updated fade">' .
		__('Error: plugin "MEO Real Estate Developments" requires a newer version of PHP to be running.',  MRED_TEXT_DOMAIN).
		'<br/>' . __('Minimal version of PHP required: ', MRED_TEXT_DOMAIN) . '<strong>' . MRED_MIN_PHP_VERSION . '</strong>' .
		'<br/>' . __('Your server\'s PHP version: ', MRED_TEXT_DOMAIN) . '<strong>' . phpversion() . '</strong>' .
		'</div>';
}

function mred_phpversioncheck() {
	if (version_compare(phpversion(), MRED_MIN_PHP_VERSION) < 0) {
		add_action('admin_notices', 'mred_showversionnotice');
		return false;
	}
	return true;
}

function mred_missingplugin() {
	global $required_plugins;
	echo '<div class="updated fade">' .
		__('Error: plugin "MEO Real Estate Developments" depends on the following" plugins.  ',  MRED_TEXT_DOMAIN).
		__('Please install and/or activate them', MRED_TEXT_DOMAIN) .
		'<br/>' . join('<br/>', $required_plugins) .
		'</div>';
}

function mred_plugindependencycheck() {
	global $required_plugins;
	if (empty($required_plugins)) {
		return true;
	}
	if (!function_exists('is_plugin_active')) {
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	}
	foreach ($required_plugins as $required_plugin) {
		if (!is_plugin_active($required_plugin)) {
			add_action('admin_notices', 'mred_missingplugin');
			return false;
		}
	}
	return true;
}

function mred_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}

function mred_admin_head() {
	$css = 'td.media-icon img[src$=".svg"] { width: 100% !important; height: auto !important; }';

	echo '<style type="text/css">'.$css.'</style>';
}

function mred_styles_and_scripts() {
	global $wp_styles;

	if ( !is_admin() ) {
		wp_enqueue_style( 'fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css' );
		wp_enqueue_style('mred', plugins_url('css/meo-real-estate-developments.css', __FILE__), array(), filemtime(dirname(__FILE__) . '/css/meo-real-estate-developments.css'));

		wp_register_style( 'mred-ie8',  plugins_url('css/meo-real-estate-developments.ie8.css', __FILE__), array('mred'), filemtime(dirname(__FILE__) . '/css/meo-real-estate-developments.ie8.css') );
		$wp_styles->add_data( 'mred-ie8', 'conditional', 'lt IE 9' );
		wp_enqueue_style( 'mred-ie8' );

		wp_enqueue_script('tablesorter', plugins_url('js/jquery.tablesorter.js', __FILE__), array('jquery'), '2.0.5b', true);

		wp_enqueue_script('superfish', plugins_url('js/superfish.min.js', __FILE__), array('jquery'), '1.7.4', true);

		wp_enqueue_script('jquery-visible', plugins_url('js/jquery.visible.js', __FILE__), array('jquery'), '1.0.0', true);

		wp_enqueue_script('mred', plugins_url('js/meo-real-estate-developments.js', __FILE__), array('tablesorter', 'superfish', 'jquery-visible'), filemtime(dirname(__FILE__) . '/js/meo-real-estate-developments.js'), true);
	}
}

function mred_localise() {
	wp_localize_script( 'mred', 'mred', array(
		'lots' => mred_get_lots(array('pdf', 'floor_position_image', 'floor_position_image_fallback', 'lot_situation_image', 'lot_situation_image_fallback')),
		'plans' => mred_get_plans(array('pdf')),
		'floors' => mred_get_floors(array('plan')),
		'sectors' => mred_get_sectors(),
		'buildings' => mred_get_buildings(),
		'lot_types' => mred_get_lot_types(),
		'entries' => mred_get_entries(),
		'translations' => array(
			'rooms' => mred_translate('Rooms'),
			'apartment' => mred_translate('Apartment'),
			'statuses' => mred_get_availability_descriptions()
		)
	) );
}

function mred_add_query_vars($qvars) {
	$qvars[] = 'lot_type';
	return $qvars;
}

function mred_load_pluggables() {
	require_once('mred-pluggable.php');
}

function mred_clear_image_cache($meta_id, $post_id, $meta_key, $meta_value) {
	if ('availability' == $meta_key || 'price' == $meta_key) {
		$lot = mred_get_lot($post_id);
		if (!empty($lot)) {
			$upload_directory = wp_upload_dir();
			$files = array(
				$upload_directory['basedir'] . '/cache/svg/' . $lot['floor_id'] . '.svg',
				$upload_directory['basedir'] . '/cache/html/' . $lot['floor_id'] . '.html',
				$upload_directory['basedir'] . '/cache/html/lot-list.html',
				$upload_directory['basedir'] . '/cache/html/lot-list-price.html'
			);
			$languages = mred_get_all_languages();
			foreach($languages as $language) {
				$files[] = $upload_directory['basedir'] . '/cache/svg/'  . $lot['floor_id'] . '.' . $language . '.svg';
				$files[] = $upload_directory['basedir'] . '/cache/html/' . $lot['floor_id'] . '.' . $language . '.html';
			}
			foreach ($files as $file) {
				if (file_exists($file)) {
					unlink ($file);
				}
			}
		}
	}
}

function mred_init() {
	$plugin_dir = dirname(plugin_basename(__FILE__));
	load_plugin_textdomain(MRED_TEXT_DOMAIN, false, $plugin_dir . '/languages/');
}

// Check dependencies - need posts 2 posts
// TODO: move P2P check into DAO - might not be needed for other implementations
// TODO: include ACF and ACF repeater in the plugin

$dependencies_present = mred_plugindependencycheck();
$dependencies_present = mred_phpversioncheck() && $dependencies_present;

// If all the dependencies are there, we're good to go
if ($dependencies_present) {
	global $mred;

	$plugin_root = plugin_dir_path( __FILE__ );

	require_once( $plugin_root . 'classes/class-meo-real-estate-developments.php');

	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('MeoRealEstateDevelopments', 'activate'));
	register_deactivation_hook(__FILE__, array('MeoRealEstateDevelopments', 'deactivate'));

	$mred = new MeoRealEstateDevelopments();

	// Ensure helper functions are defined
	require_once( $plugin_root . 'api.php');
	require_once( $plugin_root . 'shortcodes.php');

	add_action( 'wp_enqueue_scripts', 'mred_styles_and_scripts' );
	add_filter( 'upload_mimes', 'mred_mime_types' );
	add_action( 'admin_head', 'mred_admin_head');
	add_action( 'wp', 'mred_load_pluggables', 10);

	// add_action( 'wp_enqueue_scripts', 'mred_localise' );

	add_filter( 'query_vars', 'mred_add_query_vars');
	add_action( 'updated_post_meta', 'mred_load_pluggables', 9);
	add_action( 'updated_post_meta', 'mred_clear_image_cache', 10, 4);
	add_action( 'init', 'mred_init' );
	add_action('wp_ajax_get_apartment_list', array($mred, 'getAjaxLotList'));
	add_action('wp_ajax_nopriv_get_apartment_list', array($mred, 'getAjaxLotList'));
	add_action('wp_ajax_get_contact_list', array($mred, 'getAjaxContactList'));
	add_action('wp_ajax_nopriv_get_contact_list', array($mred, 'getAjaxContactList'));
	add_action('wp_ajax_get_contact', array($mred, 'getAjaxContact'));
	add_action('wp_ajax_nopriv_get_contact', array($mred, 'getAjaxContact'));
	add_action('wp_ajax_get_development', array($mred, 'getAjaxDevelopment'));
	add_action('wp_ajax_nopriv_get_development', array($mred, 'getAjaxDevelopment'));
	add_action('wp_ajax_get_statuses', array($mred, 'getAjaxStatuses'));
	add_action('wp_ajax_nopriv_get_statuses', array($mred, 'getAjaxStatuses'));
	add_action('wp_ajax_set_status', array($mred, 'setAjaxStatus'));
	add_action('wp_ajax_nopriv_set_status', array($mred, 'setAjaxStatus'));
	add_action('wp_ajax_set_price', array($mred, 'setAjaxPrice'));
	add_action('wp_ajax_nopriv_set_price', array($mred, 'setAjaxPrice'));

}
