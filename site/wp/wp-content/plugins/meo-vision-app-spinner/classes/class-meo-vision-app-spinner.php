<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoVisionAppSpinner {
	const CPT_SPINNER     = 'spinner';
	const REL_SPINNER_PLAN = 'spinners_to_plans';

	public function __construct() {
		if (!function_exists('is_plugin_active')) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}

		add_action('init', array(&$this, 'createCustomPostTypes'), 6);
		add_action('init', array(&$this, 'createSpinnerFields'), 6);
		add_action('p2p_init', array(&$this, 'createRelationships'));
	}


	public static function activate() {
		// Do nothing
	}

	public static function deactivate() {
		// Do nothing
	}

	public function getSpinner($spinner_id) {
		$result = array( 
			'id'        => $spinner_id,
			'width'     => SPINNER_IMAGE_SCREEN_WIDTH,
			'height'    => SPINNER_IMAGE_SCREEN_HEIGHT,
			'clockwise' => get_field('clockwise', $spinner_id) ? 1 : 0,
			'stretch'   => get_field('stretch', $spinner_id)   ? 1 : 0,
			'images'    => array()
		);

		$images = get_field('images', $spinner_id);
		foreach ($images as $image) {
			$result['images'][] = $image['image'];
		}

		return $result;
	}

	public function createCustomPostTypes() {
		register_post_type(self::CPT_SPINNER, array(
			'labels' => array(
				'name' => __('Spinners', MVAS_TEXT_DOMAIN),
				'singular_name' => __('Spinner', MVAS_TEXT_DOMAIN),
				'add_new_item' => __('Add a spinner', MVAS_TEXT_DOMAIN),
				'edit_item' => __('Edit spinner', MVAS_TEXT_DOMAIN),
				'new_item' => __('Add a spinner', MVAS_TEXT_DOMAIN),
				'search_items' => __('Find a spinner', MVAS_TEXT_DOMAIN),
				'not_found' => __('No spinner found', MVAS_TEXT_DOMAIN),
				'not_found_in_trash' => __('No spinner found in the trash', MVAS_TEXT_DOMAIN),
			),
			'public' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => array('title')
		));

	}


	public function createSpinnerFields() {
		register_field_group(array (
			'id' => 'acf_spinner-properties',
			'title' => 'Spinner properties',
			'fields' => $this->getSpinnerFieldDefinitions(),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => self::CPT_SPINNER,
						'order_no' => 0,
						'group_no' => 0,
					),
				),
			),
			'options' => array (
				'position' => 'normal',
				'layout' => 'no_box',
				'hide_on_screen' => array (
				),
			),
			'menu_order' => 0,
		));
	}

	public function createRelationships() {
		if (! function_exists('p2p_register_connection_type')) {
			error_log("p2p_register_connection_type function doesn't exist");
			return;
		}

		if (!class_exists('DevelopmentDaoP2P', false)) {
			// Don't have the plan declared, so can't add a relationship
			return;
		}

		$relationships = array(
			self::REL_SPINNER_PLAN => array(
				'from' => self::CPT_SPINNER,
				'to' => DevelopmentDaoP2P::CPT_PLAN,
			)
		);

		foreach ($relationships as $relationship_name => $relationship_details) {
			$args =  array(
				'name' => $relationship_name,
				'admin_column' => 'from',
				'cardinality' => 'one-to-many'
			);
			foreach ($relationship_details as $relationship_field_name => $relationship_field_value) {
				$args[$relationship_field_name] = $relationship_field_value;
			}
			p2p_register_connection_type($args);
		}
	}

	private function getSpinnerFieldDefinitions() {
		return array (
				array (
					'key' => 'field_55991cc042689',
					'label' => 'Clockwise',
					'name' => 'clockwise',
					'type' => 'true_false',
					'instructions' => 'Are the images in a clockwise order (see http://jquery.vostrel.cz/reel#options - cw)?',
					'message' => '',
					'default_value' => 1,
				),
				array (
					'key' => 'field_55991d5216bb2',
					'label' => 'Stretch',
					'name' => 'stretch',
					'type' => 'true_false',
					'instructions' => 'Resize the spinner to fill the display',
					'message' => '',
					'default_value' => 1,
				),
				array (
					'key' => 'field_55991dcb1585a',
					'label' => 'Images',
					'name' => 'images',
					'type' => 'repeater',
					'instructions' => 'NB: It\'s possible to add several images at a time via the \'Add File\' button',
					'sub_fields' => array (
						array (
							'key' => 'field_55991dda1585b',
							'label' => 'Image',
							'name' => 'image',
							'type' => 'image',
							'save_format' => 'url',
							'preview_size' => 'thumbnail',
							'library' => 'all',
						),
					),
					'row_min' => '',
					'row_limit' => '',
					'layout' => 'row',
					'button_label' => 'Add Row',
				),
			);
	}

}
