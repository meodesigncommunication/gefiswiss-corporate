<?php
require_once('class/Mobile_Detect.php');
global $wpdb;
global $post,
$mk_options;

$user_ID = get_current_user_id();
$categories = get_the_terms($post->ID, 'cat_produit');

$main_access_private = false;

$arrayFiles = array();

$actualities = array();
$detect = new Mobile_Detect;
$count = 1;


// Check Si l'utilisateur est lié à une catégorie
if(!empty($user_ID))
{
    // Recupération des liaison entre user et catégorie
    $list_droit_acces = $wpdb->get_results( 'SELECT * FROM wp_access_user_relationship WHERE user_id = '.$user_ID.' AND type="produit"');
    
    //Boucle qui parcours le resultat de la query ci-dessus
    foreach ($list_droit_acces as $result)
    {
        // Boucle qui parcours les catégories lié au post
        foreach ($categories as $category)
        {                   
            // Test pour checker si le user est lié à la catégorie
            if($result->term_id == $category->term_id)
            {
                $main_access_private = true;
            }
        }
    }
}

// Boucle qui parcours les catégories lié au post
foreach ($categories as $category)
{          
    if( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ):
 	// loop through the rows of data
        while ( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ) : the_row();

            $cat_id = $category->term_id;
            $title = get_sub_field('titre_document');
            $file = get_sub_field('fichier', false);
            $access_file = get_sub_field('acces');

            $arrayFiles[] = array(
                'cat_id' => $cat_id,
                'title' => $title,
                'file' => $file,
                'access' => $access_file,
                'permission' => $main_access_private
            );

        endwhile;
    endif;
}

// Boucle qui parcours les catégories
foreach ($categories as $category)
{
    if(!empty($catQuery))
    {
        $catQuery .= ',';
    }
    $catQuery .= $category->term_id;
}

if(empty($catQuery))
{
    $actualities = null;
}else{
    $args = array(
        'post_type'=> 'post',
        'cat' => $catQuery,
        'order'    => 'ASC'
    );
    $actualities = new WP_Query( $args );    
}

get_header('notitle'); 
?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
    <div class="mk-main-wrapper-holder">
        <div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper mk-grid vc_row-fluid">
            <div class="theme-content" itemprop="mainContentOfPage">
                <div class="wpb_row vc_inner vc_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-8">
                        <?php
                        // The Loop
                        if($detect->isMobile() && !$detect->isTablet()){
                            
                            echo '<h1>'.$post->post_title.'</h1>';
                            echo '<p>'.$post->post_content.'</p>';
                            
                            while ( have_posts() ) : the_post();
                                $args = array(
                                    'post_type' => 'produit',
                                    'post_parent' => $post->ID,
                                    'order' => 'ASC'
                                );
                                query_posts($args);
                                $shortcode .= '<ul class="meo-accordion">'; 
                                while ( have_posts() ) : the_post();
                                    $shortcode .= '<li class="meo-accordion-tab">';
                                    $shortcode .=    '<h4>'.$post->post_title.'</h4>';
                                    $shortcode .=    '<div class="tab-container"><div class="tab-body">'.$post->post_content.'</div></div>';
                                    $html .= $post->post_content."<br/><br/>";
                                    $shortcode .= '</li>';
                                endwhile;
                                $shortcode .= '</ul>';
                            endwhile;
                        }else{
                            while ( have_posts() ) : the_post();
                                the_title('<h1>','</h1>');
                                the_content();
                                $args = array(
                                    'post_type' => 'produit',
                                    'post_parent' => $post->ID,
                                    'order' => 'ASC'
                                );
                                query_posts($args);
                                // The Loop
                                $shortcode = '[vc_tta_tabs]';
                                while ( have_posts() ) : the_post();        
                                    $categories = get_the_terms($post->ID, 'cat_produit');
                                    $access_private = false;  
                                    if(!empty($categories)){
                                        if(!empty($user_ID))
                                        {
                                            //Boucle qui parcours le resultat de la query ci-dessus
                                            foreach ($list_droit_acces as $result)
                                            {
                                                // Boucle qui parcours les catégories lié au post
                                                foreach ($categories as $category)
                                                {
                                                    // Test pour checker si le user est lié à la catégorie
                                                    if($result->term_id == $category->term_id)
                                                    {
                                                        $access_private = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // Boucle qui parcours les catégories lié au post
                                    foreach ($categories as $category)
                                    {          
                                        if( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ):
                                            // loop through the rows of data
                                            while ( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ) : the_row();

                                                $cat_id = $category->term_id;
                                                $title = get_sub_field('titre_document');
                                                $file = get_sub_field('fichier', false);
                                                $access_file = get_sub_field('acces');

                                                $arrayFiles[] = array(
                                                    'cat_id' => $cat_id,
                                                    'title' => $title,
                                                    'file' => $file,
                                                    'access' => $access_file,
                                                    'permission' => $access_private
                                                );

                                            endwhile;
                                        endif;
                                    }
                                    if(get_post_status ( $post->ID ) == 'private'):
                                        if($access_private):
                                            $shortcode .= '[vc_tta_section title="'.$post->post_title.'" tab_id="1447945632-'.$count.'-7"]'.$post->post_content.'[/vc_tta_section]';
                                        endif;
                                    else:
                                        $shortcode .= '[vc_tta_section title="'.$post->post_title.'" tab_id="1447945632-'.$count.'-7"]'.$post->post_content.'[/vc_tta_section]';
                                    endif;     
                                    $count++;
                                endwhile;        
                                if(!empty($actualities->posts) && is_array($actualities->posts)):
                                    $shortcode .= '[vc_tta_section title="'.__('[:fr]Actualité[:en]News').'" tab_id="1447945632-'.$count.'-7"]';
                                    if(!empty($actualities->posts) && is_array($actualities->posts)):
                                        foreach($actualities->posts as $article):
                                            $title = $article->post_title;
                                            $content = $article->post_content; 
                                            $permalink = get_site_url().'/'.$article->post_name;
                                            $shortcode .= '<article>';
                                            $shortcode .= '<h2>'.$title.'</h2>';
                                            $shortcode .= '<div>';
                                            $shortcode .= $content;
                                            $shortcode .= '</div>';
                                            $shortcode .= '</article>';                                              
                                        endforeach;
                                    endif;                                
                                    $shortcode .= '[/vc_tta_section]';
                                endif;        
                                $shortcode .= '[/vc_tta_tabs]';   
                                wp_reset_query();
                            endwhile;
                        }
                        echo do_shortcode($shortcode);
                        //echo $shortcode;
                        wp_reset_query();
                        ?>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-4">
                        <?php 
                            $post_categories = wp_get_post_categories( $post->ID );
                            $cats = array();

                            $phone = get_field('phone');
                            $number = (!empty($phone)) ? $phone : '+41 21 613 80 70';
                            $number_link = str_replace(' ', '', $number);

                        $contact_type = get_field('contact_type');
                        $target = '_blank';

                        if($contact_type == 'mail') {
                            $email = get_field('contact_mail');
                            if(!empty($email)) {
                                $url = 'mailto:'.$email;
                                $target = '';
                            }
                        }else if($contact_type == 'url'){
                            $url = get_field('contact_url');
                        }

                        if(empty($url)) {
                            $url = '/contact';
                        }

                        ?>
                        <a target="<?php echo $target; ?>" href="<?php echo $url ?>" title="<?php echo __('[:fr]contactez nous[:en]contact us') ?>">
                            <div class="project-contact project-email">
                                <span><?php echo __('[:fr]Contactez nous[:en]Contact us'); ?></span>
                                <i class="fa fa-envelope"></i>   
                                <div class="clear-both"></div>
                            </div>
                        </a>
                        <a href="callto:<?php echo $number_link ?>" title="<?php echo __('[:fr]numéro de téléphone[:en]phone number') ?>">
                            <div class="project-contact project-phone">
                                <span><?php echo $number ?></span>
                                <i class="fa fa-phone-square"></i>  
                                <div class="clear-both"></div>
                            </div>
                        </a>
                        <?php
                            $link = get_field('lien_site');
                            if(!empty($link))
                            { ?>
                                <a target="_blank" href="<?php echo get_field('lien_site') ?>" title="<?php echo $post->post_title ?>">
                                    <div class="project-contact project-contact-web project-phone">
                                        <span>Site internet du produit</span>
                                        <i class="fa fa-link"></i>  
                                        <div class="clear-both"></div>
                                    </div>
                                </a>
                        <?php    
                        
                            } 

                            // Documentation
                            if(is_array($arrayFiles) && count($arrayFiles) > 0){

                            	echo __('[:fr]<h2 class="title-other-project sub-title">Documentation</h2>[:en]<h2 class="title-other-project sub-title">Documentation</h2>');

                            	foreach($arrayFiles as $file):

                                    $file_post = get_post($file['file']);

                                    echo '<a target="_blank" class="pdf-download-link" title="'.$file['title'].'" href="'.$file_post->guid.'">
					                            	<div class="project-contact download-file">
					                            	<span>'.$file['title'].'</span>
					                            	<i class="fa fa-link"></i>
					                            	<div class="clear-both"></div>
					                            	</div>
				                            	</a>';
                            	
	                            	/*$id_file = ($file['file']*17)+3;
	                            	
	                            	$current_url = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
	                            	$additional_parameters = 'current_url='.$current_url.'';
	                            	
	                            	if($file['access'] == 'private'):
		                            	if($file['permission']):
		                            	
		                            	if($detect->isMobile() && !$detect->isTablet())
		                            	{
		                            		echo '<a class="pdf-download-link" title="'.$file['title'].'" href="'.get_bloginfo('url').'/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'&'.$additional_parameters.'">
					                            	<div class="project-contact download-file">
					                            	<span>'.$file['title'].'</span>
					                            	<i class="fa fa-link"></i>
					                            	<div class="clear-both"></div>
					                            	</div>
				                            	</a>';
		                            	}else{
		                            		echo '<a class="fancybox-iframe pdf-download-link" title="'.$file['title'].'" href="'.get_bloginfo('url').'/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'">
					                            	<div class="project-contact download-file">
					                            	<span>'.$file['title'].'</span>
					                            	<i class="fa fa-link"></i>
					                            	<div class="clear-both"></div>
					                            	</div>
				                            	</a>';
		                            	}
		                            	endif;
		                            	else:
		                            	if($detect->isMobile() && !$detect->isTablet())
		                            	{
		                            		echo '<a class="pdf-download-link" title="'.$file['title'].'" href="'.get_bloginfo('url').'/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'&'.$additional_parameters.'">
					                            	<div class="project-contact download-file">
					                            	<span>'.$file['title'].'</span>
					                            	<i class="fa fa-link"></i>
					                            	<div class="clear-both"></div>
					                            	</div>
				                            	</a>';
		                            	}else{
		                            		echo '<a class="fancybox-iframe pdf-download-link" title="'.$file['title'].'" href="'.get_bloginfo('url').'/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'">
					                            	<div class="project-contact download-file">
					                            	<span>'.$file['title'].'</span>
					                            	<i class="fa fa-link"></i>
					                            	<div class="clear-both"></div>
					                            	</div>
				                            	</a>';
		                            	}
	                            	endif;
                            	*/
                            	
                            	endforeach;
                            }
                        
                        global $wpdb;
                        $tab_product = '';
                        $results = $wpdb->get_results( 'SELECT * FROM wp_posts WHERE post_type = "produit" AND post_parent = 0 ', OBJECT );
                        
                        foreach($results as $result)
                        {
                            $categories = get_the_terms( $result->ID, 'cat_produit' );
                            
                            if(is_array($categories) && !empty($categories)){
                                foreach($categories as $category)
                                {
                                    $status_category = get_field('status', $category->taxonomy.'_'.$category->term_id);

                                    if($status_category == 'Actif' && $result->ID <> $post->ID)
                                    {
                                        if(!empty($tab_product))
                                        {
                                            $tab_product .= ',';
                                        }
                                        $tab_product .= $result->ID;
                                    }
                                }
                            }
                        }
                        
                        $linkedProjects = get_field('linked_project');
                        if(!empty($linkedProjects)){
                        	$linkedProjectsTitle = 'Notre projet li&eacute;';
                        	if(strpos($linkedProjects, ',') !== false) {$linkedProjectsTitle = 'Nos projets li&eacute;s';}
                        ?>
                            <h2 class="title-other-project sub-title"><?php echo $linkedProjectsTitle; ?></h2>
                            <?php echo do_shortcode('[project ids='.$linkedProjects.']'); ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php bloginfo('template_directory');?>/../gefiswiss/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory');?>/../gefiswiss/js/jquery.tabbedcontent.min.js"></script>
<script type="text/javascript">
    window.$ = jQuery;
    $(function() {
        
        $(window).load(function() {
           $('div.tab-container').hide();
        });
        
        $('.meo-accordion-tab h4').click(function(){            
            if($(this).parent().hasClass('tab_active'))
            {                
                $(this).parent().removeClass('tab_active');
                $(this).parent().children('div.tab-container').hide();
            }else{
               $('div.tab-container').each(function(){
                   $(this).parent().removeClass('tab_active');
                   $(this).hide();
               }); 
               $(this).parent().addClass('tab_active');
               $(this).parent().children('div.tab-container').show();
            }            
        });
    });
</script>
<?php get_footer(); ?>