<?php
/*
Plugin Name: MEO Product User Relationship
Description: A Plugin for assign a user with multiple category 
Author: MEO
Version: 1.0
*/

/*
 * 
 * Add a table in database during plugin activation
 * 
 */
global $jal_db_version;
$jal_db_version = '1.0';

// Add page in admin navigation under user icon
// see -> https://codex.wordpress.org/Adding_Administration_Menus
add_action('admin_menu', 'my_plugin_menu_product');
function my_plugin_menu_product() {
    global $current_user;
    
    $roles = $current_user->roles;
    
    if($roles[0] <> 'investisseur')
    {
        add_users_page('Droit accès produit', 'Droit accès produit', 'read', 'user_access_product', 'user_access_product');
    }    
}

add_action( 'admin_footer', 'my_action_javascript_product' ); // Write our JS below here

function my_action_javascript_product() { ?>
    <script type="text/javascript" >
        jQuery(document).ready(function($) {   
            $('#list-product').change(function(){
                if($('#list-product').val() != 0)
                {
                    $.ajax({
                        url: ajaxurl,
                        data: {
                            'action':'category_ajax_request_product',
                            'user_id' : $('#list-user').val(),
                            'cat_id' : $('#list-product').val()
                        },
                        success:function(data) {
                            $('#bloc-user-relationship').html(data);
                        },
                        error: function(errorThrown){
                            console.log(errorThrown);
                        }
                    });
                }else{
                    alert('Veuillez sélectionner un Produit !');
                }
            });
            
            $('#save-relationship').click(function() {
                var values = new Array();
                $.each($('input[name="user_cat[]"]:checked'), function() {
                    values.push($(this).val());
                });
                $.ajax({
                    url: ajaxurl,
                    method: 'POST',
                    data: {
                        'action': 'category_ajax_save_data_product',
                        'base_cat': $('input[name="base_cat"]').val(),
                        'user_cat' : values
                    },
                    success:function(data) {
                        if(data){
                            alert('Enregistrement effectué avec succès');
                        }else{
                            alert('Une erreur c\'est produite durant l\'enregistrement');
                        }
                    },
                    error: function(errorThrown){
                        console.log(errorThrown);
                    }
                });
            });   
            
        });
    </script> 
<?php
}

function category_ajax_request_product() {
    global $wpdb;
    $table_name = $wpdb->prefix.'access_user_relationship';
    $cat_id = $_GET['cat_id'];
    $user_id = 0;
    
    $argsUser = array(
        'blog_id' => 1
    );
    if($user_id != 0)
    {
        $argsUser['ID'] = $user_id;
    }    
    
    $argsCat = array(
        'type'                     => 'post',
	'child_of'                 => $cat_id,
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'hide_empty'               => 0,
	'hierarchical'             => 1,
	'taxonomy'                 => 'cat_produit',
	'pad_counts'               => true 
    );     
    
    $base_cat =  get_term_by( 'id', $cat_id, 'cat_produit');
    
    // The Query
    $user_query = new WP_User_Query( $argsUser );    
    $categories = get_categories($argsCat); 
    $results = $wpdb->get_results('SELECT * FROM '.$table_name.' WHERE type="produit"');
    
    $html  = '<input type="hidden" name="base_cat" value="'.$base_cat->term_id.'" />';
    $html .= '<table id="tab-user-relationship">';
    $html .=    '<tr>';
    $html .=        '<td>&nbsp;</td>';
    $html .=        '<td>'.$base_cat->name.'</td>';
    
    foreach( $categories as $category ):
        if($category->name != 'Non classé'){
            $html .= '<td>'.$category->name.'</td>';
        }
    endforeach;
    
    $html .=    '</tr>';  
     
    foreach( $user_query->results as $user ):
        $html .= '<tr>';
        $html .= '<td>'. $user->display_name .'</td>';  
        
        $checked = '';
        $input_value = $user->id.'_'.$base_cat->term_id;
        foreach ($results as $result)
        {
            $info = $result->user_id.'_'.$result->term_id;
            if($info == $input_value)
            {
                $checked = 'checked=checked';
            }
        }
        $html .= '<td><input '.$checked.' type="checkbox" name="user_cat[]" value="'.$user->id.'_'.$base_cat->term_id.'" /></td>';        
        foreach( $categories as $category ):
            if($category->name != 'Non classé'){
                $checked = '';
                $input_value = $user->id.'_'.$category->cat_ID;
                foreach ($results as $result)
                {
                    $info = $result->user_id.'_'.$result->term_id;
                    if($info == $input_value)
                    {
                        $checked = 'checked=checked';
                    }
                }
                $html .= '<td><input '.$checked.' type="checkbox" name="user_cat[]" value="'.$user->id.'_'.$category->cat_ID.'" /></td>';
            }
        endforeach;
        $html .=    '</tr>';
    endforeach;
    $html .= '</table>';    
    echo $html;
    exit();
} 
add_action( 'wp_ajax_category_ajax_request_product', 'category_ajax_request_product' );


function user_access_product()
{    
    global $wpdb;  
    $args = array(
        'blog_id' => 1
    );    
    
    $argsCat = array(
	'child_of'                 => 0,
	'parent'                   => 0,
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'hide_empty'               => 0,
	'hierarchical'             => 1,
	'exclude'                  => '',
	'include'                  => '',
	'number'                   => '',
	'taxonomy'                 => 'cat_produit',
	'pad_counts'               => true 
    );   
    
    
    // The Query
    $user_query = new WP_User_Query( $args );    
    $categories = get_categories($argsCat); 
    $html .= '<div id="search-group">';  
        $html .= '<select id="list-product" name="list-product">';
        $html .=    '<option selected="true" value="0">Choisir un Produit</option>';
        foreach( $categories as $category ):
            if($category->name != 'Non classé'){
                $html .= '<option value="'. $category->cat_ID .'">'. $category->name .'</option>';
            }
        endforeach;
        $html .= '</select>';
    $html .= '</div>';
    $html .= '<div id="bloc-user-relationship">';  
    $html .= '</div>';
    $html .= '<input type="submit" name="send" id="save-relationship" value="Enregistrer" />';
    echo $html;
}

function category_ajax_save_data_product()
{
    global $wpdb;
    $table_name = $wpdb->prefix.'access_user_relationship';
    $base_cat = $_POST['base_cat'];
    $datas = $_POST['user_cat'];  
    
    //Arguments Query
    $argsUser = array(
        'blog_id' => 1
    );    
    $argsCat = array(
        'type'                     => 'post',
	'child_of'                 => $base_cat,
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'hide_empty'               => 0,
	'hierarchical'             => 1,
	'taxonomy'                 => 'cat_produit',
	'pad_counts'               => true 
    );         
    
    // The Query
    $user_query = new WP_User_Query( $argsUser );    
    $categories = get_categories($argsCat);     
    $wpdb->query("DELETE FROM `".$table_name."` WHERE term_id = ".$base_cat." AND type='produit'");    
    foreach($categories as $category)
    {   
        $wpdb->query("DELETE FROM `".$table_name."` WHERE term_id = ".$category->term_id." AND type='produit'");
    }   
    
    //Add INSERT
    if(!empty($datas)){
        foreach ($datas as $data)
        {
            $list = explode('_', $data);
            $wpdb->insert( 
                $table_name, 
                array( 
                    'user_id' => $list[0], 
                    'term_id' => $list[1], 
                    'type' => 'produit'
                ) 
            );
        }
    }
    
}
add_action( 'wp_ajax_category_ajax_save_data_product', 'category_ajax_save_data_product' );

?>