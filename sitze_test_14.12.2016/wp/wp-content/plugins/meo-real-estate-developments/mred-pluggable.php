<?php

// Pluggable functions
// May be overidden in themes to add desired functionality

// PDF download URL for a lot.
// Default behaviour: direct link to the PDF

if (!function_exists( 'mred_get_pdf_request_url')) {
	function mred_get_pdf_request_url($lot_id, $additional_parameters = '') {
		$lot = mred_get_lot($lot_id);
		if (empty($lot)) {
			return '#';
		}
		$pdf = $lot['pdf'];
		if (empty($pdf)) {
			$plan = mred_get_plan($lot['plan_id']);
			if (empty($plan)) {
				return '#';
			}
			$pdf = $plan['pdf'];
		}

		if (!empty($pdf) && isset($pdf['id'])) {
			// Use get_permalink, rather than $pdf['url'], to allow filter hooks to run
			$result = get_permalink($pdf['id']);
			if (!empty($additional_parameters)) {
				$result .= ( strpos($result, '?') === false ) ? '?' : '&';
				$result .= $additional_parameters;
			}
			return $result;
		}

		return '#';
	}
}


// Complete HTML of subheader at the top of the lot page
// Default behaviour: empty

if (!function_exists( 'mred_get_lot_subheader')) {
	function mred_get_lot_subheader($lot_id) {
		return '';
	}
}


// Complete HTML of breadcrumb at the top of the lot page
// Default behaviour: empty

if (!function_exists( 'mred_get_lot_breadcrumb')) {
	function mred_get_lot_breadcrumb($lot_id) {
		return '';
	}
}


// Show page header
// Default behaviour: WordPress standard get_header() call

if (!function_exists( 'mred_show_page_header')) {
	function mred_show_page_header() {
		get_header();
	}
}


if (!function_exists( 'mred_get_plan_filter_classes')) {
	function mred_get_plan_filter_classes($lot_id) {
		$lot = mred_get_lot($lot_id);
		$floor = mred_get_floor($lot['floor_id']);
		$building = mred_get_building($floor['building_id']);
		$sector = mred_get_sector($building['sector_id']);
		$plan = mred_get_plan($lot['plan_id']);

		$filter_classes = array(
			'building_' . $building['id'],
			'sector_' . $sector['id'],
			'floor_' . $floor['id'],
			'rooms_' . preg_replace('/\D/', '_', $lot['pieces']),
			'entry_' . $lot['entry_id'],
			'lot_type_' . $lot['type']['slug'],
			'balcony_' . ( empty($lot['surface_balcony']) ? '0' : '1' ),
			'availability_' . mred_get_availability_class($lot['availability'])
		);

		return $filter_classes;
	}
}

// Return the lot types for each building
if (!function_exists( 'mred_get_lot_types_for_building')) {
	function mred_get_lot_types_for_building() {
		$buildings = mred_get_buildings();
		$lot_types_to_buildings = array();

		foreach ($buildings as $building) {
			foreach ($building['lot_types'] as $building_lot_type) {
				$lot_types_to_buildings[$building_lot_type['id']] = array( 'building_id' => $building['id'] ); // Assumes only one building per lot type
			}
		}

		return $lot_types_to_buildings;
	}
}

// Return the appropriate lot type for the building / lot type parameter combination
// If the lot type doesn't apply to the building, return the building's default
// lot type instead
if (!function_exists( 'mred_get_lot_type_for_request')) {
	function mred_get_lot_type_for_request($building_id, $lot_type_parameter, $lot_types_to_buildings) {
		$lot_types = mred_get_lot_types();
		$buildings = mred_get_buildings();

		$lot_type_for_current_building = 0;
		foreach ($buildings as $building) {
			foreach ($building['lot_types'] as $building_lot_type) {
				if ($building['id'] == $building_id and $lot_type_for_current_building == 0) {
					$lot_type_for_current_building = $building_lot_type['id'];
				}
			}
		}

		$current_lot_type = '';
		foreach ($lot_types as $lot_type) {
			if ($lot_type['slug'] == $lot_type_parameter) {
				$current_lot_type = $lot_type['id'];
			}
		}

		if (!array_key_exists($current_lot_type, $lot_types_to_buildings) or $lot_types_to_buildings[$current_lot_type]['building_id'] != $building_id) {
			$current_lot_type = $lot_type_for_current_building;
		}

		return $current_lot_type;
	}
}

// Building/lot type filter menu
// Options can be added and removed with the building_filter_options filter
// rather than overwriting the entire function
if (!function_exists( 'mred_get_lot_type_filters')) {
	function mred_get_lot_type_filters($building_id, $current_lot_type, $lot_types_to_buildings) {
		$lot_types = mred_get_lot_types();

		$lot_types_to_buildings[$current_lot_type]['active'] = true;

		$filter_menu_options = apply_filters('building_filter_options', $lot_types_to_buildings);

		$result = '';

		$result .= '<ul class="filter-menu sf-menu">';

		foreach ($filter_menu_options as $lot_type => $filter_menu_option) {
			$name = $lot_types[$lot_type]['name'];
			$link = get_permalink($filter_menu_option['building_id']) . '?lot_type=' . $lot_types[$lot_type]['slug'];
			$classes = $lot_types[$lot_type]['slug'];
			$classes .= $filter_menu_option['active'] ? ' chosen' : '';

			if (!empty($filter_menu_option['classes'])) {
				$classes .= ' ' . $filter_menu_option['classes'];
			}

			$result .= '<li' . ( $classes ? ' class="' . $classes . '"' : '' ) . '>';
			$result .=   '<a href="' . $link . '">';
			$result .=     $name;
			$result .=   '</a>';
			$result .= '</li>';

		}

		$result .= '</ul>'; // .filter-menu

		return $result;
	}
}

// Plan filters.
// Options can be added and removed with the plan_list_filter_options filter,
// rather than overwriting the entire function
if (!function_exists( 'mred_get_plan_list_filters')) {
	function mred_get_plan_list_filters() {
		$buildings = mred_get_buildings();
		$sectors = mred_get_sectors();
		$floors = mred_get_floors();
		$lots = mred_get_lots();
		$lot_types = mred_get_lot_types();
		$entries = mred_get_entries();

		$building_options = array();
		foreach ($buildings as $building) {
			$building_options[$building['id']] = $building['code'];
		}

		$sector_options = array();
		foreach ($sectors as $sector) {
			$sector_options[$sector['id']] = $sector['code'];
		}

		$floor_options = array();
		foreach ($floors as $floor) {
			$floor_options[$floor['id']] = $floor['ordinal'] . (count($buildings) > 1 ? ' - ' . $buildings[$floor['building_id']]['name'] : '');
		}

		$entry_options = array();
		foreach ($entries as $entry) {
			$entry_options[$entry['id']] = $entry['name'];
		}

		$lot_type_options = array();
		foreach ($lot_types as $lot_type) {
			$lot_type_options[$lot_type['slug']] = $lot_type['name'];
		}

		$room_options = array();
		foreach ($lots as $lot) {
			$escaped_rooms = preg_replace('/\D/', '_', $lot['pieces']);
			$room_options[$escaped_rooms] = $lot['pieces'] . ' ' . mred_translate('p.');
		}
		asort($room_options);

		$filter_menu_options = array(
			'all' => array(
				'name' => mred_translate('All'),
				'filter_type' => 'all',
				'classes' => 'chosen'
			),
			'lot_types' => array(
				'name' => mred_translate('Type'),
				'filter_type' => 'lot_type',
				'options' => $lot_type_options
			),
			'sector' => array(
				'name' => mred_translate('sector'),
				'filter_type' => 'sector',
				'options' => $sector_options
			),
			'building' => array(
				'name' => mred_translate('Building'),
				'filter_type' => 'building',
				'options' => $building_options
			),
			'floor' => array(
				'name' => mred_translate('Floor'),
				'filter_type' => 'floor',
				'options' => $floor_options
			),
			'entry' => array(
				'name' => mred_translate('Entry'),
				'filter_type' => 'entry',
				'options' => $entry_options
			),
			'rooms' => array(
				'name' => mred_translate('Rooms'),
				'filter_type' => 'rooms',
				'options' => $room_options
			),
			'balcony' => array(
				'name' => mred_translate('Balcony'),
				'filter_type' => 'balcony',
				'options' => array(
					'0' => mred_translate('No'),
					'1' => mred_translate('Yes')
				)
			),
			'availability' => array(
				'name' => mred_translate('Availability'),
				'filter_type' => 'availability',
				'options' => mred_get_availability_descriptions()
			)
		);

		$filter_menu_options = apply_filters('plan_list_filter_options', $filter_menu_options);


		$result = '';

		$result .= '<ul class="filter-menu sf-menu">';

		foreach ($filter_menu_options as $filter_menu_option) {
			$name = $filter_menu_option['name'];
			$filter_type = $filter_menu_option['filter_type'];
			$classes = empty($filter_menu_option['classes']) ? '' : $filter_menu_option['classes'];
			$options = $filter_menu_option['options'];
			$has_children = !empty($options);
			if ($has_children) {
				$classes .= ' has-children';
			}

			$result .= '<li' . ( $classes ? ' class="' . $classes . '"' : '' ) . '>';
			if (!$has_children) {
				$result .= '<a data-filter-type="' . $filter_type . '" href="#' . $filter_type . '">';
			}
			$result .= '<span class="filter-menu-header">' . $name . '</span>';
			if ($has_children) {
				$result .= '<ul class="submenu">';
				foreach ($options as $option_value => $option_name) {
					$result .= '<li><a data-filter-type="' . $filter_type . '" data-filter-value="' . $option_value . '" href="#' . $filter_type . '_' . $option_value . '">' . $option_name . '</a></li>';
				}
				$result .= '</ul>';
			}

			if (!$has_children) {
				$result .= '</a>';
			}
			$result .= '</li>';

		}

		$result .= '</ul>'; // .filter-menu

		return $result;
	}
}

// Return a list item containing the HTML for plan portion of the list page
// Theme-specific classes (eg column classes) can be passed
if (!function_exists( 'mred_get_lot_markup_plan')) {
	function mred_get_lot_markup_plan ($lot, $floor_link, $classes = array()) {
		$floor = mred_get_floor($lot['floor_id']);
		$building = mred_get_building($floor['building_id']);
		$plan = mred_get_plan($lot['plan_id']);

		$lot_link = get_permalink($lot['id']);
		if (function_exists('qtrans_convertURL')) {
			$lot_link = qtrans_convertURL($lot_link, $current_language);
		}

		$filter_classes = mred_get_plan_filter_classes($lot['id']);

		$image = get_stylesheet_directory_uri() . '/images/transparent.gif';

		if ($plan['detailed_plan_image']) {
			 $image_details = wp_get_attachment_image_src($plan['detailed_plan_image']['id'], 'plan-small');
			 $image = $image_details[0];
		}

		if ($plan['detailed_plan_image_fallback']) {
			 $image_details = wp_get_attachment_image_src($plan['detailed_plan_image_fallback']['id'], 'plan-small');
			 $fallback_image = $image_details[0];
		}

		$lot_code = apply_filters( 'mred_get_vignette_lot_code', $lot['code'], $lot );

		ob_start();
		?>

		<li class="apartment-list-image-wrapper <?php echo join(" ", $classes); ?> <?php echo join(" ", mred_get_lot_plan_classes()); ?> <?php echo join(" ", $filter_classes); ?>">
			<div class="apartment-list-image">
				<div class="apartment_code"><a href="<?php echo $lot_link; ?>"><span class="lot_type"><?php echo $lot['type']['name']; ?> </span><?php echo $lot_code; ?></a></div>
				<div class="apartment_details">
					<a href="<?php echo $lot_link; ?>">
						<span class="rooms"><?php echo $lot['pieces']; echo '&nbsp;<span class="lower">' . mred_translate('p.') . '</span>'; ?> <span class="separator">|</span></span>
					</a>
					<a href="<?php echo $floor_link; ?>#floor_<?php echo $lot['floor_id']; ?>">
						<span class="floor"><?php echo $floor['ordinal']; ?> <span class="separator">|</span></span>
					</a>
					<a href="<?php echo $lot_link; ?>">
						<span class="area"><?php echo $lot['surface_weighted']; echo '&nbsp;<span class="lower">m</span><sup>2</sup>';  ?></span>
					</a>
				</div>
				<div class="availability-wrapper"><a class="availability" href="<?php echo $lot_link; ?>"><?php echo mred_get_availability_description($lot['availability']); ?></a></div>
				<div class="plan"><a href="<?php echo $lot_link; ?>"><img src="<?php echo $image; ?>" data-fallback="<?php echo $fallback_image; ?>" alt="<?php echo $lot['code']; ?>"></a></div>
				<div class="pdf-download"><?php echo mred_get_download_button($lot['id']); ?></div>
			</div>
			<a href="<?php echo $lot_link; ?>" class="apartment-list-overlay" style="display: none;">&nbsp;</a>
		</li>
		<?php

		return apply_filters( 'mred_get_lot_markup_plan', ob_get_clean() );
	}
}

// Return a list item containing the HTML for list portion the list page
// Theme-specific classes can be passed
if (!function_exists( 'mred_get_lot_markup_list')) {
	function mred_get_lot_markup_list ($lot, $floor_link, $classes = array()) {
		$floor = mred_get_floor($lot['floor_id']);
		$building = mred_get_building($floor['building_id']);
		$lot_type = empty($lot['type']) ? '' : $lot['type']['slug'];
		$lot_link = get_permalink($lot['id']);
		if (function_exists('qtrans_convertURL')) {
			$lot_link = qtrans_convertURL($lot_link, $current_language);
		}

		$filter_classes = mred_get_plan_filter_classes($lot['id']);

		ob_start();
		?>

		<li class="apartment-list-row <?php echo join(" ", $classes); ?> <?php echo join(" ", mred_get_lot_list_classes()); ?> <?php echo join(" ", $filter_classes); ?>">
			<ul>
				<li class="apartment_code_and_availability">
					<a href="<?php echo $floor_link; ?>#floor_<?php echo $lot['floor_id']; ?>">
						<div class="apartment_code"><?php echo $lot['code']; ?></div>
						<div class="availability"><?php echo mred_get_availability_description($lot['availability']); ?></div>
					</a>
				</li>
				<li class="building"><a href="<?php echo $floor_link; ?>#floor_<?php echo $lot['floor_id']; ?>"><span class="list_icon list_icon_building"></span><span class="list_building_text"><?php echo $building['code']; ?></span></a></li>
				<li class="floor"><a href="<?php echo $floor_link; ?>#floor_<?php echo $lot['floor_id']; ?>"><span class="list_icon list_icon_floor"></span><span class="list_building_text"><?php echo $floor['ordinal']; ?></span></a></li>
				<li class="rooms"><a href="<?php echo $lot_link; ?>"><span class="list_icon list_icon_rooms"></span><span class="list_building_text"><?php echo $lot['pieces']; echo '&nbsp;<span class="lower">' . mred_translate('p.') . '</span>'; ?></span></a></li>
				<li class="area"><a href="<?php echo $lot_link; ?>"><span class="list_icon list_icon_area"></span><span class="list_building_text"><?php echo $lot['surface_weighted']; echo '&nbsp;<span class="lower">m</span><sup>2</sup>';  ?></span></a></li>
				<?php if (!empty($lot['pdf'])) { ?>
					<li class="pdf-download"><a class="fancybox-iframe pdf-download-link" href="<?php echo mred_get_pdf_request_url($lot['id']); ?>"><?php echo mred_translate('Info pack'); ?></a></li>
				<?php } else { ?>
					<li class="pdf-download"><a class="see-lot-link" href="<?php echo $lot_link; ?>"><?php echo mred_translate('View lot details'); ?></a></li>
				<?php } ?>
			</ul>
		</li><?php

		return apply_filters( 'mred_get_lot_markup_list', ob_get_clean() );
	}
}


// Get classes that apply to the plan par list items (container)
if (!function_exists( 'mred_get_lot_thumb_wrapper_classes')) {
	function mred_get_lot_thumb_wrapper_classes() {
		return array('wpb_row', 'vc_row', 'vc_row-fluid', 'mk-fullwidth-true', 'attched-false ');
	}
}

// Get classes that apply to the plan par list items (columns)
if (!function_exists( 'mred_get_lot_plan_classes')) {
	function mred_get_lot_plan_classes() {
		return array('vc_span3', 'wpb_column', 'column_container');
	}
}

// Get classes that apply to the list items
if (!function_exists( 'mred_get_lot_list_classes')) {
	function mred_get_lot_list_classes() {
		return array();
	}
}



// Get download button HTML
if (!function_exists( 'mred_get_download_button')) {
	function mred_get_download_button($lot_id) {
		global $download_lot_id;
		$download_lot_id = $lot_id;

		$lot = mred_get_lot($lot_id);
		if (!empty($lot['pdf'])) {
			return '<a class="fancybox-iframe pdf-download-link" href="' .  mred_get_pdf_request_url($lot_id) . '">' . mred_translate('Download Info pack') .'</a>';
		}

		$lot_link = get_permalink($lot_id);
		return '<a class="see-lot-link" href="' . $lot_link .'">' . mred_translate('View lot details') .'</a>';
	}
}


// Get projectview logo
if (!function_exists( 'mred_get_projectview_row')) {
	function mred_get_projectview_row() { ?>
		<div class="projectview-re-logo">
			<img src="<?php echo mred_get_plugin_images_url(); ?>/projectviewlogo.png" />
		</div><?php
	}
}

// Display building / floor selector
if (!function_exists( 'mred_get_building_floor_selector')) {
	function mred_get_building_floor_selector($building_id, $building_floors, $floor_index, $options = array()) {
		$options = mred_extract_options( array(
			'buildings-wrapper' => array(
				'classes' => array()
			),
			'buildings' => array(
				'classes' => array()
			),
			'building-wrapper' => array(
				'classes' => array()
			),
			'building-wrapper-inner' => array(
				'classes' => array()
			),
			'sections' => array() // contains arrays containing function names
		), $options );

		$result .= '<div class="buildings-wrapper ' . join(' ', $options['buildings-wrapper']['classes']) . '">';
		$result     .= '<div class="buildings ' . join(' ', $options['buildings']['classes']) . '">';

		$floor_heights_css = array();

		$building = mred_get_building($building_id);

		$top_floor_offset = (float) $building['top_floor_offset'];
		$css_floor_height = (float) $building['css_floor_height'];

		$floor_counter = 0;
		foreach ($building_floors as $floor) {
			$floor_heights_css[] = ".building-wrapper .floor_" . $floor["id"] . "{ top: " . preg_replace("/,/", ".", ( $top_floor_offset + $css_floor_height * $floor_counter)) . "%; height: " . preg_replace("/,/", ".", $css_floor_height ) . "% }";
			$floor_counter++;
		}

		ob_start();
		?>

		<div class="building-wrapper building-wrapper_<?php echo $building['id']; ?> <?php echo join(' ', $options['building-wrapper']['classes']); ?>">
			<div class="building-wrapper-inner <?php echo join(' ', $options['building-wrapper-inner']['classes']); ?>">
				<?php foreach ($options['sections'] as $section) {
					$section['function']($building, $building_floors, $floor_index);
				} ?>
				<div class="clear"></div>
			</div>
		</div><?php

		$result .= ob_get_clean();


		$result .= '<style type="text/css">' . join("\n", $floor_heights_css) . '</style>';

		$result     .= '</div>'; // buildings
		$result .= '</div>'; // buildings-wrapper

		return $result;
	}
}

// Display floor plans scroller
if (!function_exists( 'mred_get_floor_scroller')) {
	function mred_get_floor_scroller($building_id, $floors, $floor_index = 0, $options = array()) {
		$options = mred_extract_options( array(
			'plans-by-floor' => array(
				'classes' => array()
			),
			'floor-container' => array(
				'classes' => array()
			),
			'building-wrapper' => array(
				'classes' => array()
			),
			'building-wrapper-inner' => array(
				'classes' => array()
			),
			'sections' => array() // contains arrays containing function names
		), $options );

		wp_enqueue_script('touchwipe', plugins_url('js/jquery.touchwipe.1.1.1.js', __FILE__), array(), '1.1.1', true);

		// Add touchwipe to mred dependencies
		if (!empty($wp_scripts->registered['mred'])) {
			$wp_scripts->registered['mred']->deps[] = 'touchwipe';
		}

		$floor_ids = array();
		foreach ($floors as $floor) {
			$floor_ids[] = $floor['id'];
		}

		wp_localize_script( 'mred', 'floor_ids', $floor_ids);

		ob_start();

		?>
		<div class="plans-by-floor <?php echo join(' ', $options['plans-by-floor']['classes']); ?>">
			<div class="floor-container <?php echo join(' ', $options['floor-container']['classes']); ?>">

				<?php foreach ($options['sections'] as $section) { ?>
					<div class="floor-container-column <?php echo join(' ', $section['classes']); ?>">
						<?php $section['function']($building_id, $floors, $floor_index); ?>
					</div>
				<?php } ?>
			</div>
		</div>
		<?php

		$result = ob_get_clean();

		return $result;
	}
}

// Display floor plans scroller
if (!function_exists( 'mred_floor_scroll_container')) {
	function mred_floor_scroll_container($building_id, $floors, $floor_index) { ?>
		<div class="scroll-container">
			<?php foreach ($floors as $floor) { ?>
				<div class="floor-image-wrapper"><?php
					echo  mred_png_floor_plan_html($floor['id']);
				?></div>
			<?php } ?>
		</div><?php
	}
}

if (!function_exists( 'mred_get_language')) {
	function mred_get_language() {
		return 'fr';
	}
}

if (!function_exists( 'mred_get_all_languages')) {
	function mred_get_all_languages() {
		return array('fr');
	}
}


// Generate HTML for the PNG version of the floor plan
if (!function_exists( 'mred_png_floor_plan_html')) {
	function mred_png_floor_plan_html($floor_id) {
		$floor = mred_get_floor($floor_id);
		if (empty($floor)) {
			return '';
		}

		$upload_directory = wp_upload_dir();
		$cache_dir = $upload_directory['basedir'] . '/cache/html';
		$current_language = mred_get_language();
		$cache_file = $cache_dir . '/' . $floor['id'] . '.' . $current_language . '.html';

		wp_mkdir_p($cache_dir);
		if (file_exists($cache_file)) {
			return file_get_contents($cache_file);
		}


		require_once WP_CONTENT_DIR . "/plugins/meo-real-estate-developments/classes/svglib/svglib.php";

		$lots = mred_get_lots();
		$plans = mred_get_plans();
		$floor_lots = array();
		$svg_ids = array();
		foreach ($lots as $lot) {
			if (!array_key_exists($floor_id, $lot['all_floors'])) {
				continue;
			}

			if ($lot['all_floors'][$floor_id]['svg_id']) {
				$lot['svg_id'] = $lot['all_floors'][$floor_id]['svg_id'];
			}

			if (empty($lot['svg_id'])) {
				$lot['svg_id'] = $plans[$lot['plan_id']]['svg_id'];
			}

			$floor_lots[] = $lot;
			$svg_ids[$lot['svg_id']] = 1;
		}

		$plan_file = str_replace($upload_directory['baseurl'], $upload_directory['basedir'], $floor['plan']['url']);
		$floor_plan = SVGDocument::getInstance( $plan_file );

		$svg_id_points = array();
		foreach ($svg_ids as $svg_id => $dummy) {
			$svg_id_element = $floor_plan->getElementById( $svg_id );
			if (empty($svg_id_element)) {
				continue;
			}

			$svg_id_points[strtolower($svg_id)] = mred_get_svg_element_points($svg_id_element);
		}

		$image_base_url = get_stylesheet_directory_uri() . '/images/plans/';

		$floor_walls_image = $floor['walls_image']['url'];
		$floor_name = strip_tags($floor['name']);

		$image_map_url = $floor['floor_overlay_image']['url'];
		$image_map_file_details = wp_get_attachment_metadata( $floor['floor_overlay_image']['id'], true );
		$image_size = getimagesize(WP_CONTENT_DIR . '/uploads/' .  $image_map_file_details['file']);


		ob_start();

		?>
		<div class="floor-wrapper">
		   <img class="floor-walls" src="<?php echo $floor_walls_image; ?>" alt="<?php echo $floor_name ?>" />
		   <img class="floor-overlay-transparent" src="<?php echo $image_map_url; ?>" alt="<?php echo $floor_name ?>" width="<?php echo $image_size[0]; ?>" height="<?php echo $image_size[1]; ?>" usemap="#floor_image_map_<?php echo $floor_id; ?>" />
		   <map id="floor_image_map_<?php echo $floor_id; ?>" name="floor_image_map_<?php echo $floor_id; ?>" class="floor_image_map"><?php
				foreach ($floor_lots as $lot) {

					$lot_url = get_permalink($lot['id']);
					if (function_exists('qtrans_convertURL')) {
						$lot_url = qtrans_convertURL($lot_url, $current_language);
					}
					$points = $svg_id_points[strtolower($lot['svg_id'])];
					$coords = "";

					foreach ($points as $point) {
						if ($coords) {
							$coords .= ",";
						}
						$coords .= ((int) $point['x']) . "," . ((int) $point['y']);
					} ?>
					<area data-over="<?php echo $lot['slug']; ?>_<?php echo $lot['svg_id']; ?>" href="<?php echo $lot_url; ?>" alt="<?php echo $lot['name']; ?>" shape="polygon" coords="<?php echo $coords; ?>"><?php
				} ?>
			</map>
			<?php foreach ($floor_lots as $lot) {
				if (!array_key_exists($floor_id, $lot['all_floors'])) {
					continue;
				}

				$off_image = $lot['svg_id'] . "-" . $lot['availability'] . ".png";
				$over_image = $lot['svg_id'] . "-" . $lot['availability'] . "-hover.png";
				$availability = function_exists('mred_get_availability_description') ? mred_get_availability_description($lot['availability']) : $lot['availability'];
				?>
				<img id="overlay_<?php echo $lot['slug']; ?>_<?php echo $lot['svg_id']; ?>" class="floor_overlay" src="<?php echo $image_base_url; ?><?php echo $off_image; ?>" data-off="<?php echo $image_base_url; ?><?php echo $off_image; ?>" data-over="<?php echo $image_base_url; ?><?php echo $over_image; ?>" />
				<div id="label_overlay_<?php echo $lot['slug']; ?>_<?php echo $lot['svg_id']; ?>" class="label_<?php echo strtolower($lot['svg_id']); ?> availability-label-wrapper availability-label-wrapper_<?php echo $lot['availability']; ?>">
					<?php mred_get_lot_label($lot, $availability, $floor_id); ?>
				</div>
			<?php } ?>
		</div><?php

		$result = ob_get_clean();

		// Save the result
		$can_cache = true;
		$fh = fopen($cache_file, 'w') or $can_cache = false;
		if ($can_cache) {
			fwrite($fh, $result);
			fclose($fh);
		}

		return $result;
	}
}

// Generate HTML for the PNG version of the floor plan
if (!function_exists( 'mred_get_lot_label')) {
	function mred_get_lot_label($lot, $availability_description, $floor_id) { ?>
		<div class="lot-and-rooms"><span class="lot_code"><?php echo $lot['code']; ?></span> | <span class="rooms"><?php echo $lot['pieces']; ?> <?php _e('p', MRED_TEXT_DOMAIN); ?></div>
		<div class="availability-label"><?php echo $availability_description; ?></div><?php
	}
}
