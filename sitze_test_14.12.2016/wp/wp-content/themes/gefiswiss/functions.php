<?php

/*
 * Add a new role
 */
$role = 'investisseur';
$display_name = __('Investisseur');
$capabilities = array(
    'read' => true,
    'read_private_pages' => true,
    'read_private_posts' => true
);

add_role( $role, $display_name, $capabilities );

/*
 * Add a custom post type 
 */
add_action( 'init', 'create_post_type' );
function create_post_type() {
    register_post_type( 'projet',
        array(
            'labels' => array(
            'name' => __( 'Projets' ),
            'singular_name' => __( 'Projet' )
        ),
        'hierarchical' => true,
        'description' => 'Page projet',
        'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', 'revisions'),
        'taxonomies' => array('cat_projet'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'page'
        )
    );
}

function add_custom_taxonomies_projet() {
  register_taxonomy('cat_projet', 'projet', array(
    'hierarchical' => true,
    'labels' => array(
      'name' => _x( 'Projet Cat&eacute;gories', 'taxonomy general name' ),
      'singular_name' => _x( 'Projet Cat&eacute;gorie', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Projet Cat&eacute;gories' ),
      'all_items' => __( 'All Projet Cat&eacute;gories' ),
      'parent_item' => __( 'Parent Projet Cat&eacute;gorie' ),
      'parent_item_colon' => __( 'Parent Projet Cat&eacute;gorie:' ),
      'edit_item' => __( 'Edit Projet Cat&eacute;gorie' ),
      'update_item' => __( 'Update Projet Cat&eacute;gorie' ),
      'add_new_item' => __( 'Add New Projet Cat&eacute;gorie' ),
      'new_item_name' => __( 'New Projet Cat&eacute;gorie Name' ),
      'menu_name' => __( 'Projet Cat&eacute;gories' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'categories-projet', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
}
add_action( 'init', 'add_custom_taxonomies_projet', 0 );


/*
 * Add a custom post type 
 */
add_action( 'init', 'create_post_type_product' );
function create_post_type_product() {
    register_post_type( 'produit',
        array(
            'labels' => array(
            'name' => __( 'Produits' ),
            'singular_name' => __( 'Produit' )
        ),
        'hierarchical' => true,
        'description' => 'Page produit',
        'supports' => array('title', 'editor', 'thumbnail', 'page-attributes', 'revisions'),
        'taxonomies' => array('cat_produit'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'page'
        )
    );
}

function add_custom_taxonomies_produit() {
  register_taxonomy('cat_produit', 'produit', array(
    'hierarchical' => true,
    'labels' => array(
      'name' => _x( 'Produit Cat&eacute;gories', 'taxonomy general name' ),
      'singular_name' => _x( 'Produit Cat&eacute;gorie', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Produit Cat&eacute;gories' ),
      'all_items' => __( 'All Produit Cat&eacute;gories' ),
      'parent_item' => __( 'Parent Produit Cat&eacute;�gorie' ),
      'parent_item_colon' => __( 'Parent Produit Cat&eacute;�gorie:' ),
      'edit_item' => __( 'Edit Produit Cat&eacute;gorie' ),
      'update_item' => __( 'Update Produit Cat&eacute;gorie' ),
      'add_new_item' => __( 'Add New Produit Cat&eacute;gorie' ),
      'new_item_name' => __( 'New Produit Cat&eacute;gorie Name' ),
      'menu_name' => __( 'Produit Cat&eacute;gories' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'categories-produit', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
}
add_action( 'init', 'add_custom_taxonomies_produit', 0 );

/*
 * Add a custom post type 
 */
add_action( 'init', 'create_temoignage' );
function create_temoignage() {
    register_post_type( 'temoignage',
        array(
            'labels' => array(
            'name' => __( 'Temoignage' ),
            'singular_name' => __( 'Temoignage' )
        ),
        'public' => true
        )
    );
}


//add_action('user_category_save','insert_data');
/*function insert_data() {
    global $wpdb;
    
    $user_id = 1;
    $term_id = 1;
    
    $table_name = $wpdb->prefix.'access_user_relationship';
    $wpdb->insert( 
        $table_name, 
        array( 
            'user_id' => $user_id, 
            'term_id' => $term_id,
            'type' => ''
        ) 
    );
}*/

/**
 * Register Widget Area.
 *
 */
function toolbar_widgets_init() {

	register_sidebar( array(
		'name' => 'Toolbar area',
		'id' => 'toolbar_area',
		'before_widget' => '<div id="toolbar-widget">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="rounded">',
		'after_title' => '</h2>',
	) );
}
add_action( 'widgets_init', 'toolbar_widgets_init' );

#############################################
/* Configure Advanced Custom Fields plugin */
#############################################
define('ACF_OPTION_PAGE_STUB', 'acf-options-gefiswiss-settings');

if( function_exists('acf_add_options_page') ) {
	// Note - if changing this, the ACF_OPTION_PAGE_STUB will need to change
	acf_add_options_page('Gefiswiss Settings');
}

if( function_exists('register_field_group') ) {
    
	register_field_group(array (
		'key' => 'group_54dccd889a11f',
		'title' => 'Gefiswiss Settings',
		'fields' => array (
			array (
				'key' => 'field_54dccdaa6f49f',
				'label' => 'Logo',
				'name' => 'logo',
				'prefix' => '',
				'type' => 'image',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'array',
				'preview_size' => 'medium',
				'library' => 'all',
			),
			array (
				'key' => 'field_54dccddc6f4a0',
				'label' => 'Adresse',
				'name' => 'address',
				'prefix' => '',
				'type' => 'textarea',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'maxlength' => '',
				'rows' => 4,
				'new_lines' => 'br',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_54dcce096f4a1',
				'label' => 'Phone',
				'name' => 'phone',
				'prefix' => '',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
			array (
				'key' => 'field_54dcce126f4a2',
				'label' => 'email',
				'name' => 'email',
				'prefix' => '',
				'type' => 'email',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
			array (
				'key' => 'field_54dcce1b6f4a3',
				'label' => 'Adresse web',
				'name' => 'url',
				'prefix' => '',
				'type' => 'text',
				'instructions' => 'Without http://',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'options_page',
					'operator' => '==',
					'value' => ACF_OPTION_PAGE_STUB,
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
	));
}

// [project ids="1,2,3,45,789" type="....."]
function project_func( $atts ){
    $html = '';    
    $a = shortcode_atts( array(
        'ids' => '0',
    	'type' => 'ALL'
    ), $atts ); 
    $list_id = explode(',', $a['ids']);
    
	if( $a['ids'] != '0' && is_array($list_id) && count($list_id) > 0 ) {
	    $args = array(
	        'post_type' => 'projet',
	        'post__in' => $list_id,
	        'post_parent' => 0
	    );
    }
    else {
    	$args = array(
    			'post_type' => 'projet',
    			'post_parent' => 0
    	);
    }
    
    if($a['type'] && $a['type'] != 'ALL'){
    	$accepted_types = array();
    	$types_options = get_field_object('field_56c444d5f9129', 'projet'); // Project / Product Type
    	foreach($types_options['choices'] as $choice){
    		$accepted_types[] = $choice;
    	}
    	
    	if(in_array($a['type'], $accepted_types)){
	    	$args['meta_query']	= array(
	    			'relation'		=> 'AND',
	    			array(
	    					'key'	 	=> 'type',
	    					'value'	  	=> html_entity_decode($a['type']),
	    					'compare' 	=> '=',
	    			)
	    	);
    	}
    }
    $accents = array(' ', '&eacute', '&Eacute;', '&egrave', '&Egrave', '&agrave', '&Agrave');
    $noaccents = array('-', 'e', 'e', 'e', 'e', 'a', 'a');
    $type_class = str_replace($accents, $noaccents, strtolower(html_entity_decode($a['type'])));
    
    
    $query = new WP_Query($args);
    if ( $query->have_posts() ) :
        while( $query->have_posts() ) : $query->the_post();
            $featuredImageId = get_post_thumbnail_id($query->post->ID);        
            $imageUrl = wp_get_attachment_image_src($featuredImageId, 'full');
            $style = 'style="background: transparent url('.$imageUrl[0].') no-repeat; background-position: center; background-size: cover;"';
            $html .= '<a class="project-link-shortcode project-vignette '.$type_class.'" href="'.$query->post->guid.'" title="'.$query->post->post_title.'"><article class="project-shortcode" '.$style.'>';
            $html .=    '<div class="project-bg">';
            $html .=        '<div class="project-align-center">';
            $html .=            '<div class="txt-project">';
            $html .=                '<h2>'.$query->post->post_title.'</h2>';
            $html .=                '<h3>'.get_field('localite',$project->ID).'</h3>';
            $html .=            '</div>';
            $html .=        '</div>';
            $html .=    '</div>';
            $html .= '</article></a>';
        endwhile;
    endif;
    // Reset Query
    wp_reset_query();
    return $html;
}
add_shortcode( 'project', 'project_func' );

// [product ids="1,2,3,45,789" type="..."]
function product_func( $atts ){
    $html = '';    
    $a = shortcode_atts( array(
        'ids' => '0',
    	'type' => 'ALL'
    ), $atts ); 
    $list_id = explode(',', $a['ids']);
    
    if( $a['ids'] != '0' && is_array($list_id) && count($list_id) > 0 ) {
	    $args = array(
	        'post_type' => 'produit',
	        'post__in' => $list_id,
	        'post_parent' => 0
	    );
    }
    else {
    	$args = array(
    			'post_type' => 'produit',
    			'post_parent' => 0
    	);
    }
    
    if($a['type'] && $a['type'] != 'ALL'){
    	$accepted_types = array();
    	$types_options = get_field_object('field_56c444d5f9129', 'produit'); // Project / Product Type
    	foreach($types_options['choices'] as $choice){
    		$accepted_types[] = $choice;
    	}
    	
    	if(in_array($a['type'], $accepted_types)){
    		$args['meta_query']	= array(
    				'relation'		=> 'AND',
    				array(
    						'key'	 	=> 'type',
    						'value'	  	=> html_entity_decode($a['type']),
    						'compare' 	=> '=',
    				)
    		);
    		
    	}
    }
    $accents = array(' ', '&eacute', '&Eacute;', '&egrave', '&Egrave', '&agrave', '&Agrave');
    $noaccents = array('-', 'e', 'e', 'e', 'e', 'a', 'a');
    $type_class = str_replace($accents, $noaccents, strtolower(html_entity_decode($a['type'])));
    /*
    $gradientArray = array(
    						'scpc' => 'rgba(52, 101, 127, 0.6), rgba(52, 101, 127, 0.6)',
    						'sicav' => 'rgba(140, 198, 225, 0.6), rgba(140, 198, 225, 0.6)',
    						'fondation' => 'rgba(233, 237, 242, 0.6), rgba(233, 237, 242, 0.6)',
    						'si' => 'rgba(232, 196, 34, 0.6), rgba(232, 196, 34, 0.6)',
    						'mandats-prives' => 'rgba(50, 50, 51, 0.6), rgba(50, 50, 51, 0.6)',
    );
    */
    $query = new WP_Query($args);
    if ( $query->have_posts() ) :
        while( $query->have_posts() ) : $query->the_post();
    
            $featuredImageId = get_post_thumbnail_id($query->post->ID);        
            $imageUrl = wp_get_attachment_image_src($featuredImageId, 'full');
            
            // Get specific type if not set in the shortcode
            if($a['type'] == 'ALL'){
            	$postType = get_field_object('field_56c444d5f9129', $query->post->ID); // Project / Product Type
            	$type_class = str_replace($accents, $noaccents, strtolower(html_entity_decode($postType['value'])));
            }
            
            if(!empty($imageUrl)){ //  linear-gradient('.$gradientArray[$type_class].'),
                $style = 'style="background: url('.$imageUrl[0].') no-repeat; background-position: center; background-size: cover;"';
            }else{
                $style = 'style="background: url('.get_bloginfo('url').'/wp/wp-content/uploads/2015/12/no-pics_2.jpg) no-repeat; background-position: center; background-size: cover;"';
            }
            
            $html .= '<a class="project-link-shortcode product-vignette '.$type_class.'" href="'.$query->post->guid.'" title="'.$query->post->post_title.'"><article class="project-shortcode" '.$style.'>';
            $html .=    '<div class="project-bg">';
            $html .=        '<div class="project-align-center">';
            $html .=            '<div class="txt-project">';
            $html .=                '<h2>'.$query->post->post_title.'</h2>';
            $html .=                '<h3>'.get_field('localite',$project->ID).'</h3>';
            $html .=            '</div>';
            $html .=        '</div>';
            $html .=    '</div>';
            $html .= '</article></a>';
        endwhile;
    endif;
    // Reset Query
    wp_reset_query();
    return $html;
}
add_shortcode( 'product', 'product_func' );

// [homeproducts ids="1,2,3,45,789" type="..." limit="xxx"]
function homepage_products( $atts ){
	$html = '';
	$a = shortcode_atts( array(
			'ids' => '0',
			'type' => 'ALL',
			'limit' => '1000'
	), $atts );
	$list_id = explode(',', $a['ids']);
	$limit = intval($a['limit']); // Limit the number of printed results, not queried results
	
	if( $a['ids'] != '0' && is_array($list_id) && count($list_id) > 0 ) {
		$args = array(
				'post_type' => 'produit',
				'post__in' => $list_id,
				'post_parent' => 0,
                'order' => 'DESC'
		);
	}
	else {
		$args = array(
				'post_type' => 'produit',
				'post_parent' => 0,
                'order' => 'DESC'
		);
	}

	if($a['type'] && $a['type'] != 'ALL'){
		$accepted_types = array();
		$types_options = get_field_object('field_56c444d5f9129', 'produit'); // Project / Product Type
		foreach($types_options['choices'] as $choice){
			$accepted_types[] = $choice;
		}
		 
		if(in_array($a['type'], $accepted_types)){
			$args['meta_query']	= array(
					'relation'		=> 'AND',
					array(
							'key'	 	=> 'type',
							'value'	  	=> html_entity_decode($a['type']),
							'compare' 	=> 'LIKE',
					)
			);

		}
	}
	$accents = array(' ', '&eacute', '&Eacute;', '&egrave', '&Egrave', '&agrave', '&Agrave');
	$noaccents = array('-', 'e', 'e', 'e', 'e', 'a', 'a');
	$type_class = str_replace($accents, $noaccents, strtolower(html_entity_decode($a['type'])));
	/*
	$gradientArray = array(
			'scpc' => 'rgba(52, 101, 127, 0.6), rgba(52, 101, 127, 0.6)',
			'sicav' => 'rgba(140, 198, 225, 0.6), rgba(140, 198, 225, 0.6)',
			'fondation' => 'rgba(233, 237, 242, 0.6), rgba(233, 237, 242, 0.6)',
			'si' => 'rgba(232, 196, 34, 0.6), rgba(232, 196, 34, 0.6)',
			'mandats-prives' => 'rgba(50, 50, 51, 0.6), rgba(50, 50, 51, 0.6)',
	);
	*/
	
	$products = query_posts($args);
	$count = 0;
	
	// The Loop
	$html = '<div class="row">';
	$printed_results = 0;
	foreach ($products as $product)
	{
		if($printed_results == $limit) {break;}
		
		if($count >= 3)
		{
			$html .= '</div>';
			$html .= '<div class="row">';
			$count = 0;
		}
	
		$categories = get_the_terms($product->ID, 'cat_produit');
		foreach ($categories as $category)
		{
			$status_category = get_field_object('field_56a8684a56be3', 'cat_produit_'.$category->term_id); // produit status
	
			if($status_category['value'] == 'Actif'){
				$class = 'global_project status_on';
			}else if($status_category['value'] == 'Inactif'){
				$class = 'status_off';
			}else{
				$class = 'no_status';
			}
		}
	
		$featuredImageId = get_post_thumbnail_id($product->ID);
		$imageUrl = wp_get_attachment_image_src($featuredImageId, 'full');
		
		// Get specific type if not set in the shortcode
		if($a['type'] == 'ALL'){
			$postType = get_field_object('field_56c444d5f9129', $product->ID); // Project / Product Type
			$type_class = str_replace($accents, $noaccents, strtolower(html_entity_decode($postType['value'])));
		}
		
		$categories = get_the_terms($product->ID, 'cat_produit');
		$category = $categories[0];
		if(!empty($imageUrl)){ //  linear-gradient('.$gradientArray[$type_class].'), 
			$style = 'style="background:url('.$imageUrl[0].') no-repeat; background-position: center; background-size: cover;"';
		}else{
			$style = 'style="background: url('.get_bloginfo('url').'/wp/wp-content/uploads/2015/12/no-pics_2.jpg) no-repeat; background-position: center; background-size: cover;"';
		}
	
		// $status = get_field('status', $category->taxonomy.'_'.$category->term_id);
		$status = get_field_object('field_56a8684a56be3', 'cat_produit_'.$category->term_id); // produit status
	
		$printProduit = false;
		if($status['value'] == 'En cours de souscription')
		{
			$type_cat = "yellow-cat";
			$printProduit = true;
		}else if(substr($status['value'], 0, 7) == 'Capital'){ //  entièrement souscris
			$type_cat = "light-blue-cat";
			$printProduit = false;
		}else{
			$type_cat = "blue-cat";
			$printProduit = true;
		}
	
		if($printProduit){
			$printed_results++;
			
			$html .= '<div class="col-md-4 '.$class.'">';
			$html .=    '<article class="bloc-project">';
			$html .=        '<div '.$style.' class="vignette-img">';
			if(get_field( "agree_finma", $product->ID ) == 'oui'){
				$html .= '<div class="bandeau-finma">&nbsp;</div>';
			}

			if(get_field( "agree_chs_pp", $product->ID ) == 'oui'){
				$html .= '<div class="bandeau-chs_pp">&nbsp;</div>';
			}
			$html .=            '<div class="vignette-status '.$type_cat.'">'.get_field('status', $category->taxonomy.'_'.$category->term_id).'</div>';
			$html .=        '</div>';
			$html .=        '<div class="vignette">';
			$html .=            '<h2>'.$product->post_title.'</h2>';
			$html .=            '<p>'.substr($product->post_content,0,100).' [...]</p>';
			$html .=            '<a href="'.$product->guid.'" title="'.$product->post_title.'">';
			$html .=                '<input type="button" class="btn btn-default btn-outline-border-color" value="'.__('[:fr]En savoir plus[:en]Read more').'">';
			$html .=            '</a>';
			$html .=        '</div>';
			$html .=    '</article>';
			$html .= '</div>';
			 
			$count++;
		}
	}
	$html .= '</div>';
	// Reset Query
	wp_reset_query();
	return $html;
}
add_shortcode( 'homeproducts', 'homepage_products' );

/* Add a CSS wp-admin.css pour cacher les menu du plugin real-estate */
function my_custom_css() {    
    echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/wp-admin.css" type="text/css" media="all" />';  
}
add_action('admin_head', 'my_custom_css');


function shortcodes_in_cf7( $form ) {
    $form = do_shortcode( $form );
    return $form;
}
add_filter( 'wpcf7_form_elements', 'shortcodes_in_cf7' );

/* Shortcode to generate input hidden [inputPreviousUrl url ]
 * [inputPreviousUrl url=""] */
	add_shortcode( 'inputPreviousUrl', 'inputPreviousUrl_shortcode' );

  	function inputPreviousUrl_shortcode() {
            $url = '';
            if(isset($_GET['current_url']) && !empty($_GET['current_url']))
            {
                $url = 'http://'.$_GET['current_url'];
            }
	    return '<input type="hidden" name="pervious_url" value="'.$url.'">';
	}        
/* Shortcode to generate button cancel for file request page on mobile device [buttonCancelPreviousUrl url ]
 * [buttonCancelPreviousUrl url=""] */
        add_shortcode( 'buttonCancelPreviousUrl', 'buttonCancelPreviousUrl_shortcode' );

  	function buttonCancelPreviousUrl_shortcode() {
            
            $url = '';
            $html = '';
            
            if(isset($_GET['current_url']) && !empty($_GET['current_url']))
            {
                $url = 'http://'.$_GET['current_url'];
                $html = '<a class="wpcf7-button-cancel" href="'.$url.'"><input type="button" value="Annuler" /></a>';
            }
            
            return $html;
	}
        
/* Shortcode to generate button price list [buttonPriceList url]
 * [buttonPriceList url=""] */
        add_shortcode( 'buttonPriceList', 'buttonPriceList_shortcode' );

  	function buttonPriceList_shortcode($atts) {
            
            $a = shortcode_atts( array(
	        'url' => '#'
	    ), $atts );
            
            require_once('class/Mobile_Detect.php');
            $detect = new Mobile_Detect();
            
            if(!$detect->isMobile())
            {
                $class = 'fancybox-iframe btn-download-list-price';
                $url = $a['url'];
            }else{
                $current_url = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
                if($detect->isTablet()){
                    $class = 'fancybox-iframe btn-download-list-price';
                    $url = $a['url'];
                }else{
                    $class = 'btn-download-list-price';
                    $url = $a['url'].'&current_url='.$current_url;
                }
            }    
            
            $html = '<a class="'.$class.'" href="'.$url.'">&nbsp;</a>';
            
            return $html;
	}
        
/* Shortcode to generate a projects slider in footer [projectSlider]
 * [projectSlider] */
        add_shortcode( 'projectSlider', 'project_slider_shortcode' );
        
        function project_slider_shortcode($atts) {            
            global $wp;
            
            $args = array(
                'post_type' => 'projet',
                'post_parent' => 0
            );             
            $projects = query_posts($args);
            
            $html = '';  
            if ( !empty($projects) ) :         
                $count = 0;
                $html .= '<div id="footer-project" class="swiper-container">';
                $html .=    '<div class="swiper-wrapper">'; 
                    foreach($projects as $project):
                        
                        $categories = get_the_terms($project->ID, 'cat_projet');
                        $category = $categories[0];
                        
                        $field = get_field('status', $category->taxonomy.'_'.$category->term_id);
                        if($field == 'En cours')
                        {
                            $featuredImageId = get_post_thumbnail_id($project->ID);        
                            $imageUrl = wp_get_attachment_image_src($featuredImageId, 'full');
                            $html .=    '<div class="swiper-slide"><a href="'.get_bloginfo('home').'/'.$project->post_name.'" title="'.$project->post_title.'">';
                            $html .=        '<img src="'.$imageUrl[0].'" alt="'.$project->post_title.'" />';
                            $html .=    '</a></div>';
                        }
                        
                    endforeach;
                $html .=    '</div>';
                $html .=    '<div class="swiper-button-prev swiper-button-white"></div>';
                $html .=    '<div class="swiper-button-next swiper-button-white"></div>';
                $html .= '</div>';  
                
                $html .= '<script type="text/javascript">';
                $html .=    'window.$ = jQuery;';
                $html .=    'function heightFooterProject(){';
                $html .=        'var height = $("#footer-project").width()/1.777777;';
                $html .=        '$("#footer-project").height(height);';
                $html .=        '$("#footer-project").css("overflow","hidden");';
                $html .=    '}';
                $html .=    '$(document).ready(function(){';  
                $html .=        'heightFooterProject();';
                $html .=    '});';                
                $html .=    '$( window ).resize(function() {';
                $html .=        'heightFooterProject();';
                $html .=    '});'; 
                $html .= '</script>';
            endif;
            
            return $html;
	}
        
        add_filter('cn_cookie_notice_output','openCookieProject',1,1);
        
        function openCookieProject($output)
        {
            global $post;
            // if($post->post_type == 'projet' && !is_page() && !is_front_page() && !is_home())
            // Print disclaimer when visiting these pages
            $allowedPages = array(1476, 2510, 2100, 2307, 1733, 2612, 1279, 1861, 2712);
            if(in_array($post->ID, $allowedPages))
            {
                return $output;
            }
            
            return '';
        }
/* Shortcode to generate a testimonial list[testimonial]
 * [temoignage] */
add_shortcode( 'temoignage', 'temoignage_shortcode' );

function temoignage_shortcode($atts) {

    global $wpdb;
    
    $results = $wpdb->get_results( 'SELECT * FROM wp_posts WHERE post_type = "temoignage" AND post_status = "publish" ORDER BY RAND() LIMIT 1', OBJECT );
    
    foreach($results as $result)
    {
        echo '<div class="testimonial">';
        echo '<p>"'.$result->post_content.'"</p>';
        echo '<p class="signature"><span class="editeur">'.get_field('editeur',$result->ID).'</span><br/>';
        echo '<span class="job">'.get_field('job__compagny',$result->ID).'</span></p>';
        echo '</div>';
    }
}

/* Shortcode to generate a testimonial [testimonial]
 * [testimonial ids=""] */
add_shortcode( 'testimonial','temoignage_multiple_shortcode');

function temoignage_multiple_shortcode($atts) {
    $a = shortcode_atts( array(
        'ids' => '0'
    ), $atts );     
    $list_id = explode(',', $a['ids']);    
    $args = array(
        'post_type' => 'temoignage',
        'post__in' => $list_id,
        'post_parent' => 0
    );
    $query = new WP_Query($args);  
    if ( $query->have_posts() ) :
        while( $query->have_posts() ) : $query->the_post();
            echo '<div class="testimonial">';
            echo '<p>"'.$query->post->post_content.'"</p>';
            echo '<p class="signature"><span class="editeur">'.get_field('editeur',$query->post->ID).'</span><br/>';
            echo '<span class="job">'.get_field('job__compagny',$query->post->ID).'</span></p>';
            echo '</div>';    
        endwhile;
    endif;
    // Reset Query
    wp_reset_query();
}

/* Ajax */

/* Project Filters */
add_action( 'wp_ajax_project_filter', 'project_filter' );
add_action( 'wp_ajax_nopriv_project_filter', 'project_filter' );

function project_filter(){
	global $wpdb;
	// Status
	$accepted_status = array(0, 1, 2);
	$status = intval( $_POST['status'] );
	if(!in_array($status, $accepted_status)){wp_die();};
	
	// Vehicules
	$vehicules = $_POST['vehicules'];
	$accepted_vehicules = array('ALL');
	$vehicules_options = get_field_object('field_56c444d5f9129', 'projet'); // Project / Product Type
    foreach($vehicules_options['choices'] as $choice){
    	$accepted_vehicules[] = str_replace(' ', '_', $choice);
    }
    if(!in_array($vehicules, $accepted_vehicules)){wp_die();};
	
	$args = array(	'post_type' => 'projet',
					'post_parent' => 0,
					'order' => 'ASC',
                                        'post__not_in' => array(3380),
					'post_status' => 'publish'
			 );
	if($vehicules != 'ALL'){
		$args['meta_query']	= array(
				'relation'		=> 'AND',
				array(
						'key'	 	=> 'type',
						'value'	  	=> str_replace('_', ' ',html_entity_decode($vehicules)),
						'compare' 	=> '=',
				)
		);
	}
	
	$projects = query_posts($args);
	$count = 0;
	
	// The Loop
	$html = '<div class="row">';
	 
	foreach ($projects as $project)
	{
		if($count >= 3)
		{
			$html .= '</div>';
			$html .= '<div class="row">';
			$count = 0;
		}
	
		$categories = get_the_terms( $project->ID, 'cat_projet' );
		
		$requestedStatusProject = false;
		
		foreach ($categories as $category)
		{
			$status_category = get_field_object('field_565c0f6a98eea', 'cat_projet_'.$category->term_id); // project status
			
			switch($status){
				case "1":
					if($status_category['value'] == 'En cours'){$requestedStatusProject = true;}
				break;
				
				case "2":
					if(substr($status_category['value'], 0, 6) == 'Termin'){$requestedStatusProject = true;}
				break;
				
				default:
					$requestedStatusProject = true;
				break;
			}
		}
		
		if($requestedStatusProject){
			$featuredImageId = get_post_thumbnail_id($project->ID);
			$imageUrl = wp_get_attachment_image_src($featuredImageId, 'full');
			$style = 'style="background: transparent url('.$imageUrl[0].') no-repeat; background-position: center; background-size: cover;"';
            $postType = get_field_object('field_56c444d5f9129', $project->ID); // Project / Product Type
		
			$html .= '<div class="col-md-4">';
			$html .=    '<article class="bloc-project">';
			$html .=        '<div '.$style.' class="vignette-img">';
			$html .=        '</div>';
			$html .=        '<div class="vignette">';
            $html .=            '<h3 class="h3-project-location">'.get_field('localite',$project->ID).'</h3>';
			$html .=            '<h2>'.$project->post_title.'</h2>';
            $html .=            '<h3 class="h3-project">'.strtoupper(html_entity_decode($postType['value'])).' | '.htmlentities($status_category['value']).'</h3>';
            /*$html .=            '<p>'.substr(html_entity_decode(htmlentities($project->post_content)),0,101).' [...]</p>';*/
            $explodedContent = array();
            $explodedContent = explode(' ', $project->post_content);
             
            $html .=            '<p>';
	            for($i=0;$i<=15;$i++){
	            	if(strpos($explodedContent[$i], '[vc_row') !== false) {
                    	$html .= str_replace('[vc_row', '', $explodedContent[$i]). ' ';
                    	break;
                    }
                    $html .= $explodedContent[$i]. ' ';
	            }
            $html .=            ' [...]</p>';
			$html .=            '<a href="'.$project->guid.'" title="'.$project->post_title.'">';
			$html .=                '<input type="button" class="btn btn-default btn-outline-border-color" value="'.__('[:fr]En savoir plus[:en]Read more').'">';
			$html .=            '</a>';
			$html .=        '</div>';
			$html .=    '</article>';
			$html .= '</div>';
		
			$count++;
		}
	}
	$html .= '</div>';
	echo $html;
	// Reset Query
	wp_reset_query();
	
	wp_die();
	
}

/* Search Filter */
function searchfilter($query) {
	if ($query->is_search && !is_admin() ) {

		$query->set('post_status', 'publish');

	}
	return $query;
}
add_filter('pre_get_posts','searchfilter');

/* Override Mk title */
function mk_page_title() {
	global $mk_options;

	$post_id = global_get_post_id();

	if (is_singular('product') && $mk_options['woocommerce_single_product_title'] == 'false') {
		return false;
	}
	if (is_singular('employees')) {
		return false;
	}

	if ($post_id && (get_post_meta($post_id, '_template', true) == 'no-title' || get_post_meta($post_id, '_template', true) == 'no-header-title' || get_post_meta($post_id, '_template', true) == 'no-header-title-footer' || get_post_meta($post_id, '_template', true) == 'no-footer-title')) {
		return false;
	}
	if ((global_get_post_id() && get_post_meta($post_id, '_enable_slidehsow', true) == 'true') || is_404()) {
		return false;
	}

	$align = $title = $subtitle = $shadow_css = '';

	if ($post_id) {
		$custom_page_title = get_post_meta($post_id, '_custom_page_title', true);
		if (!empty($custom_page_title)) {
			$title = $custom_page_title;
		}
		else {
			$title = get_the_title($post_id);
		}
		$subtitle = get_post_meta($post_id, '_page_introduce_subtitle', true);
		$align = get_post_meta($post_id, '_introduce_align', true);
	}

	/* Loads Archive Page Headings */
	if (is_archive()) {
		$title = $mk_options['archive_page_title'];
		if (is_category()) {
			$subtitle = sprintf(__('Category Archive for: "%s"', 'mk_framework') , single_cat_title('', false));
		}
		elseif (is_tag()) {
			$subtitle = sprintf(__('Tag Archives for: "%s"', 'mk_framework') , single_tag_title('', false));
		}
		elseif (is_day()) {
			$subtitle = sprintf(__('Daily Archive for: "%s"', 'mk_framework') , get_the_time('F jS, Y'));
		}
		elseif (is_month()) {
			$subtitle = sprintf(__('Monthly Archive for: "%s"', 'mk_framework') , get_the_time('F, Y'));
		}
		elseif (is_year()) {
			$subtitle = sprintf(__('Yearly Archive for: "%s"', 'mk_framework') , get_the_time('Y'));
		}
		elseif (is_author()) {
			if (get_query_var('author_name')) {
				$curauth = get_user_by('slug', get_query_var('author_name'));
			}
			else {
				$curauth = get_userdata(get_query_var('author'));
			}
			$subtitle = sprintf(__('Author Archive for: "%s"', 'mk_framework') , $curauth->nickname);
		}
		elseif (is_tax()) {
			$term = get_term_by('slug', get_query_var('term') , get_query_var('taxonomy'));
			$subtitle = sprintf(__('Archives for: "%s"', 'mk_framework') , $term->name);
		}
		if ($mk_options['archive_disable_subtitle'] == 'false') {
			$subtitle = '';
		}
	}

	if (function_exists('is_bbpress') && is_bbpress()) {
		if (bbp_is_forum_archive()) {
			$title = bbp_get_forum_archive_title();
		}
		elseif (bbp_is_topic_archive()) {
			$title = bbp_get_topic_archive_title();
		}
		elseif (bbp_is_single_view()) {
			$title = bbp_get_view_title();
		}
		elseif (bbp_is_single_forum()) {

			$forum_id = get_queried_object_id();
			$forum_parent_id = bbp_get_forum_parent_id($forum_id);

			//if ( 0 !== $forum_parent_id )
			//$title = breadcrumbs_plus_get_parents( $forum_parent_id );

			$title = bbp_get_forum_title($forum_id);
		}
		elseif (bbp_is_single_topic()) {
			$topic_id = get_queried_object_id();
			$title = bbp_get_topic_title($topic_id);
		}
		elseif (bbp_is_single_user() || bbp_is_single_user_edit()) {

			$title = bbp_get_displayed_user_field('display_name');
		}
	}

	if (function_exists('is_woocommerce') && is_woocommerce() && !empty($post_id)) {
		ob_start();
		woocommerce_page_title();
		$title = ob_get_clean();
	}

	if (function_exists('is_woocommerce') && is_woocommerce()) {
		$title = __('Shop', 'mk_framework');
		if (is_archive() || is_singular('product')) {
			$title = (isset($mk_options['woocommerce_category_page_title']) && !empty($mk_options['woocommerce_category_page_title'])) ? $mk_options['woocommerce_category_page_title'] : __('Shop', 'mk_framework');
		}
	}

	/* Loads Search Page Headings */
	if (is_search()) {
		$title = 'R&eacute;sultats de Recherche'; // $mk_options['search_page_title'];
		$allsearch = new WP_Query("s=" . get_search_query() . "&showposts=-1");
		$count = $allsearch->post_count;
		wp_reset_query();
		// $count . ' ' . 
		$subtitle = sprintf(__('Votre recherche: "%s"', 'mk_framework') , stripslashes(strip_tags(get_search_query())));

		if ($mk_options['search_disable_subtitle'] == 'false') {
			$subtitle = '';
		}
	}
	if ($mk_options['page_title_shadow'] == 'true') {
		$shadow_css = 'mk-drop-shadow';
	}

	$align = !empty($align) ? $align : 'left';

	echo '<section id="mk-page-introduce" class="intro-' . $align . '">';
	echo '<div class="mk-grid">';
	if (!empty($title)) {
		echo '<h1 class="page-introduce-title ' . $shadow_css . '">' . $title . '</h1>';
	}

	if (!empty($subtitle)) {
		echo '<div class="page-introduce-subtitle">';
		echo $subtitle;
		echo '</div>';
	}
	if ($mk_options['disable_breadcrumb'] == 'true') {
		if (get_post_meta($post_id, '_disable_breadcrumb', true) != 'false') {
			do_action('theme_breadcrumbs', $post_id);
		}
	}

	echo '<div class="clearboth"></div></div></section>';
}

/* Revue de presse Filters */
add_action( 'wp_ajax_portfolio_filter', 'portfolio_filter' );
add_action( 'wp_ajax_nopriv_portfolio_filter', 'portfolio_filter' );

function portfolio_filter(){    
    
    global $wpdb;
    global $mk_options;
    
    // Search Parameters
    $searchInput = $_POST['searchInput'];
    $project = $_POST['project'];
    $year = $_POST['year'];
    $type_media = $_POST['type_media'];
    
    // Search Query
    $select = 'SELECT p.ID';
    $from   = 'FROM wp_posts AS p';
    $join   = 'INNER JOIN wp_postmeta AS pm ON pm.post_id = p.ID';
    $where  = 'WHERE p.post_type="portfolio"';
    $groupby = 'GROUP BY p.ID';
    
    if(!empty($searchInput))
    {
        $where .= ' AND (p.post_title LIKE "%'.$searchInput.'%"';
        $where .= ' OR p.post_content LIKE "%'.$searchInput.'%"';
        $where .= ' OR (pm.meta_key="_custom_page_title" AND pm.meta_value LIKE "%'.$searchInput.'%")';
        $where .= ' OR (pm.meta_key="_page_introduce_subtitle" AND pm.meta_value LIKE "%'.$searchInput.'%"))';
    }    
    if(!empty($project))
    {
        $where .= ' AND (pm.meta_key = "projet_relation" AND pm.meta_value LIKE "%'.$project.'%")';
    }    
    if(!empty($year))
    {
        $where .= ' AND (pm.meta_key = "date_de_publication" AND pm.meta_value LIKE "'.$year.'%")';
    }    
    if(!empty($type_media))
    {
        $where .= ' AND (pm.meta_key = "type_de_media" AND pm.meta_value LIKE "'.$type_media.'")';
    }
    
    $query = $select.' '.$from.' '.$join.' '.$where.' '.$groupby;  
    $posts = $wpdb->get_results($query); 
    
    $list_posts = '';
    
    foreach($posts as $post){
        if(!empty($list_posts)){ 
            $list_posts .= ',';
        }
        $list_posts .= $post->ID;
    }
        
    $loop_style = $mk_options['archive_portfolio_style']; 
    $column = $mk_options['archive_portfolio_column'];
    $pagination_style = $mk_options['archive_portfolio_pagination_style'];
    $image_height = $mk_options['archive_portfolio_image_height'];
    
    wp_reset_query();    
    
    if(!empty($list_posts)){
        echo do_shortcode( '[mk_portfolio posts="'.$list_posts.'" style="'.$loop_style.'" column="'.$column.'" height="'.$image_height.'" pagination_style="'.$pagination_style.'"]' );
    }else{
        echo '<p style="padding-left:10px;padding-bottom:70px;">Aucuns articles trouvés</p>';
    }
    die();
}