<?php

global $mk_options;

$allsearch = new WP_Query("s=" . get_search_query() . "&showposts=-1");
$count = $allsearch->post_count;
wp_reset_query();
$subtitle = $count . ' ' . sprintf(__('Résultats de la recherche pour: "%s"', 'mk_framework') , stripslashes(strip_tags(get_search_query())));

get_header(); ?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
	<div class="mk-main-wrapper-holder">
		<div class="theme-page-wrapper <?php echo $mk_options['search_page_layout'] ?>-layout  mk-grid vc_row-fluid row-fluid">
			<div class="theme-content">
                                <?php echo do_shortcode('[mk_fancy_title tag_name="h1" color="#938f7f" size="16" txt_transform="uppercase" margin_bottom="0" font_family="none" el_class="remove-margin-bottom margin-top-none"]Recherche[/mk_fancy_title]'); ?>
                                <?php echo do_shortcode('[mk_fancy_title color="#00869b" size="35" font_weight="300" font_style="normal" txt_transform="uppercase" margin_bottom="30" font_family="none" el_class="remove-margin-top sub-title"]'.$subtitle.'[/mk_fancy_title]'); ?>
				<section class="mk-search-loop">
					<section class="widget widget_search"><p><?php _e('Pas satisfait du résultat ? Entrer de nouveaux mots clés', 'mk_framework'); ?></p>
					<form class="mk-searchform" method="get" id="searchform" action="<?php echo home_url(); ?>">
					<input type="text" class="text-input" placeholder="<?php _e('Rechercher', 'mk_framework'); ?>" value="" name="s" id="s" />
					<i class="mk-icon-search"><input value="" class="search-button" type="submit" /></i>
					</form>
					</section>
				<?php

					if ( have_posts() ):
					while ( have_posts() ) :
						the_post();

					$post_type =  get_post_type();
					?>

						<article class="search-result-item">
							<h4 class="the-title">
								<?php if($post_type == 'employees') { ?>
									<a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
								<?php } else{ ?>
									<a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a>
								<?php }	?>
							</h4>
							<div class="search-loop-meta">
								<!--<div class="mk-blog-author"><?php _e('By', 'mk_framework'); ?> <?php the_author_posts_link(); ?></div>-->
								<?php if($post_type != 'page') : ?>
								<time datetime="<?php the_time( 'F, j' ); ?>">
									<?php _e('Le', 'mk_framework'); ?> <a href="<?php echo get_month_link( get_the_time( "Y" ), get_the_time( "m" ) ); ?>"><?php the_time( 'M. t, h:i A' ); ?></a>
								</time>
								<?php endif; ?>
								<?php if($post_type == 'post') { ?>
								<span class="mk-search-cats"><?php echo get_the_category_list( ', ' ); ?></span>

							<?php } elseif ($post_type == 'portfolio') { 
									$terms = get_the_terms(get_the_id(), 'portfolio_category');
									$terms_slug = array();
									$terms_name = array();
									if (is_array($terms)) {
										foreach($terms as $term) {
											$terms_name[] = $term->name;
												}
									}

								?>	

								<span class="mk-search-cats"><?php echo implode(', ', $terms_name); ?></span>
								<?php } elseif($post_type == 'news'){
									$terms = get_the_terms(get_the_id(), 'news_category');
									$terms_slug = array();
									$terms_name = array();
									if (is_array($terms)) {
										foreach($terms as $term) {
											$terms_name[] = $term->name;
												}
									}
									?>
									<span class="mk-search-cats"><?php echo implode(', ', $terms_name); ?></span>
								<?php }	?>


								
							</div>
							<div class="the-excerpt"><p><?php mk_excerpt_max_charlength(200) ?></p></div>
						</article>

					

					<?php
					$post_type = '';
					endwhile;
					mk_post_pagination(NULL, $paged);
					wp_reset_query();
					endif;
					

					
				?>

			</section>
			</div>

		<?php if ( $mk_options['search_page_layout'] != 'full' ) get_sidebar(); ?>
		<div class="clearboth"></div>
		</div>
	</div>	
</div>
<?php get_footer(); ?>
