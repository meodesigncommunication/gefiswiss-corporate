<?php
require_once('class/Mobile_Detect.php');
global $wpdb;
global $post,
$mk_options;

$user_ID = get_current_user_id();
$categories = get_the_terms($post->ID, 'cat_projet');

$main_access_private = false;

$arrayFiles = array();

$actualities = array();
$detect = new Mobile_Detect;
$count = 1;


// Check Si l'utilisateur est lié à une catégorie
if(!empty($user_ID))
{
    // Recupération des liaison entre user et catégorie
    $list_droit_acces = $wpdb->get_results( 'SELECT * FROM wp_access_user_relationship WHERE user_id = '.$user_ID.' AND type="projet"');
    
    //Boucle qui parcours le resultat de la query ci-dessus
    foreach ($list_droit_acces as $result)
    {
        // Boucle qui parcours les catégories lié au post
        foreach ($categories as $category)
        {                   
            // Test pour checker si le user est lié à la catégorie
            if($result->term_id == $category->term_id)
            {
                $main_access_private = true;
            }
        }
    }
}

// Boucle qui parcours les catégories lié au post
foreach ($categories as $category)
{          
    if( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ):
 	// loop through the rows of data
        while ( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ) : the_row();

            $cat_id = $category->term_id;
            $title = get_sub_field('title_documentation');
            $file = get_sub_field('fichier', false);
            $access_file = get_sub_field('acces');

            $arrayFiles[] = array(
                'cat_id' => $cat_id,
                'title' => $title,
                'file' => $file,
                'access' => $access_file,
                'permission' => $main_access_private
            );

        endwhile;
    endif;
}

// Boucle qui parcours les catégories
foreach ($categories as $category)
{
    if(!empty($catQuery))
    {
        $catQuery .= ',';
    }
    $catQuery .= $category->term_id;
}

if(empty($catQuery))
{
    $actualities = null;
}else{
    $args = array(
        'post_type'=> 'post',
        'cat' => $catQuery,
        'order'    => 'ASC'
    );
    $actualities = new WP_Query( $args );    
}

get_header('notitle'); 
?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
    <div class="mk-main-wrapper-holder">
        <div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper mk-grid vc_row-fluid">
            <div class="theme-content" itemprop="mainContentOfPage">
                <div class="wpb_row vc_inner vc_row vc_row-fluid">
                    <div class="wpb_column vc_column_container vc_col-sm-8">
                        <?php
                        echo '<a href="'.get_site_url().'/?p=12#filter-realisation" title="retour au réalisation" id="btn-back-rea"><i class="fa fa-angle-left" style="padding-right: 10px"></i>Retour aux réalisations</a>';
                        // The Loop
                        if($detect->isMobile() && !$detect->isTablet()){
                            
                            the_title('<h1>','</h1>');
                            echo do_shortcode($post->post_content);
                            
                            while ( have_posts() ) : the_post();
                                $args = array(
                                    'post_type' => 'projet',
                                    'post_parent' => $post->ID,
                                    'order' => 'ASC'
                                );
                                query_posts($args);
                                $shortcode .= '<ul class="meo-accordion">'; 
                                $ii = 0;
                                while ( have_posts() ) : the_post();
                                	$ii++;
                                	$complement = '';
                                	if($ii == 1) $complement = ' tab_active';
                                    $shortcode .= '<li class="meo-accordion-tab '.$complement.'">';
                                    $shortcode .=    '<h4>'.$post->post_title.'</h4>';
                                    $shortcode .=    '<div class="tab-container"><div class="tab-body">'.$post->post_content.'</div></div>';
                                    $html .= $post->post_content."<br/><br/>";
                                    $shortcode .= '</li>';
                                endwhile;
                                $shortcode .= '</ul>';
                                
                                // Current Project Type
                                $currentPostType = get_field_object('field_56c444d5f9129', $post->ID); // Project / Product Type
                                
                            endwhile;
                        }else{
                            while ( have_posts() ) : the_post();
	
	                            // Current Project Type
	                            $currentPostType = get_field_object('field_56c444d5f9129', $post->ID); // Project / Product Type
	                            
                                the_title('<h1>','</h1>');
                                the_content();
                                $args = array(
                                    'post_type' => 'projet',
                                    'post_parent' => $post->ID,
                                    'order' => 'ASC'
                                );
                                query_posts($args);
                                // The Loop
                                $shortcode = '[vc_tta_tabs]';
                                while ( have_posts() ) : the_post();        
                                    $categories = get_the_terms($post->ID, 'cat_projet');
                                    $access_private = false;  
                                    if(!empty($categories)){
                                        if(!empty($user_ID))
                                        {
                                            //Boucle qui parcours le resultat de la query ci-dessus
                                            foreach ($list_droit_acces as $result)
                                            {
                                                // Boucle qui parcours les catégories lié au post
                                                foreach ($categories as $category)
                                                {
                                                    // Test pour checker si le user est lié à la catégorie
                                                    if($result->term_id == $category->term_id)
                                                    {
                                                        $access_private = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // Boucle qui parcours les catégories lié au post
                                    if(!empty($categories))
                                    {
                                        foreach ($categories as $category)
                                        {          
                                            if( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ):
                                                // loop through the rows of data
                                                while ( have_rows('documentation', $category->taxonomy.'_'.$category->term_id) ) : the_row();

                                                    $cat_id = $category->term_id;
                                                    $title = get_sub_field('title_documentation');
                                                    $file = get_sub_field('fichier', false);
                                                    $access_file = get_sub_field('acces');

                                                    $arrayFiles[] = array(
                                                        'cat_id' => $cat_id,
                                                        'title' => $title,
                                                        'file' => $file,
                                                        'access' => $access_file,
                                                        'permission' => $access_private
                                                    );

                                                endwhile;
                                            endif;
                                        }
                                    }
                                    if(get_post_status ( $post->ID ) == 'private'):
                                        if($access_private):
                                            $shortcode .= '[vc_tta_section title="'.$post->post_title.'" tab_id="1447945632-'.$count.'-7"]'.$post->post_content.'[/vc_tta_section]';
                                        endif;
                                    else:
                                        $shortcode .= '[vc_tta_section title="'.$post->post_title.'" tab_id="1447945632-'.$count.'-7"]'.$post->post_content.'[/vc_tta_section]';
                                    endif;     
                                    $count++;
                                endwhile;        
                                if(!empty($actualities->posts) && is_array($actualities->posts)):
                                    $shortcode .= '[vc_tta_section title="'.__('[:fr]Actualit&eacute;[:en]News').'" tab_id="1447945632-'.$count.'-7"]';
                                    if(!empty($actualities->posts) && is_array($actualities->posts)):
                                        foreach($actualities->posts as $article):
                                            $title = $article->post_title;
                                            $content = $article->post_content; 
                                            $permalink = get_site_url().'/'.$article->post_name;
                                            $shortcode .= '<article>';
                                            $shortcode .= '<h2>'.$title.'</h2>';
                                            $shortcode .= '<div>';
                                            $shortcode .= $content;
                                            $shortcode .= '</div>';
                                            $shortcode .= '</article>';                                              
                                        endforeach;
                                    endif;                                
                                    $shortcode .= '[/vc_tta_section]';
                                endif;        
                                $shortcode .= '[/vc_tta_tabs]';   
                                wp_reset_query();
                            endwhile;
                        }
                        echo do_shortcode($shortcode);
                        //echo $shortcode;
                        wp_reset_query();
                        ?>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-4">                        
                        <a target="_blank" href="http://gefiswiss.ch/contact/" title="<?php echo __('[:fr]contactez nous[:en]contact us') ?>">
                            <div class="project-contact project-email">
                                <span><?php echo __('[:fr]Contactez nous[:en]Contact us'); ?></span>
                                <i class="fa fa-envelope"></i>   
                                <div class="clear-both"></div>
                            </div>
                        </a>
                        <a href="callto:+41216138070" title="<?php echo __('[:fr]num&eacute;ro de t&eacute;l&eacute;phone[:en]phone number') ?>">
                            <div class="project-contact project-phone">
                                <span>+41 21 613 80 70</span>
                                <i class="fa fa-phone-square"></i>  
                                <div class="clear-both"></div>
                            </div>
                        </a>
                        <?php
                            $link = get_field('lien_site');
                            if(!empty($link))
                            { ?>
                                <a target="_blank" href="<?php echo get_field('lien_site') ?>" title="<?php echo $post->post_title ?>">
                                    <div class="project-contact project-contact-web project-phone">
                                        <span>Site internet du projet</span>
                                        <i class="fa fa-link"></i>  
                                        <div class="clear-both"></div>
                                    </div>
                                </a>
                        <?php    
                        
                            } 
                            
                            $post_categories = get_the_terms( $post->ID, 'cat_projet' );
                            $cats = array();
                            
                            // Documentation
                            if(is_array($arrayFiles) && count($arrayFiles) > 0){

                            	echo __('[:fr]<h2 class="title-other-project sub-title">Documentation</h2>[:en]<h2 class="title-other-project sub-title">Documentation</h2>');
                            	
	                            foreach($arrayFiles as $file):
	                                
	                                $id_file = ($file['file']*17)+3;
	                            
	                                $current_url = $_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
	                                $additional_parameters = 'current_url='.$current_url.'';
	                            
	                                if($file['access'] == 'private'):
	                                    if($file['permission']):
	                                        if($detect->isMobile() && !$detect->isTablet())
	                                        {                                                    
	                                            echo '<a class="pdf-download-link" title="'.$file['title'].'" href="'.get_bloginfo('url').'/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'&'.$additional_parameters.'">
						                            	<div class="project-contact download-file">
						                            	<span>'.$file['title'].'</span>
						                            	<i class="fa fa-file-pdf-o"></i>
						                            	<div class="clear-both"></div>
						                            	</div>
					                            	</a>';
	                                        }else{
	                                            echo '<a class="fancybox-iframe pdf-download-link" title="'.$file['title'].'" href="'.get_bloginfo('url').'/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'">
						                            	<div class="project-contact download-file">
						                            	<span>'.$file['title'].'</span>
						                            	<i class="fa fa-file-pdf-o"></i>
						                            	<div class="clear-both"></div>
						                            	</div>
					                            	</a>';
	                                        }
	                                    endif;                                       
	                                else:
	                                    if($detect->isMobile() && !$detect->isTablet())
	                                    {                                                    
	                                        echo '<a class="pdf-download-link" title="'.$file['title'].'" href="'.get_bloginfo('url').'/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'&'.$additional_parameters.'">
						                            	<div class="project-contact download-file">
						                            	<span>'.$file['title'].'</span>
						                            	<i class="fa fa-file-pdf-o"></i>
						                            	<div class="clear-both"></div>
						                            	</div>
					                            	</a>';
	                                    }else{
	                                        echo '<a class="fancybox-iframe pdf-download-link" title="'.$file['title'].'" href="'.get_bloginfo('url').'/file-request/?file_id='.$id_file.'&post_id='.$post->ID.'">
						                            	<div class="project-contact download-file">
						                            	<span>'.$file['title'].'</span>
						                            	<i class="fa fa-file-pdf-o"></i>
						                            	<div class="clear-both"></div>
						                            	</div>
					                            	</a>';
	                                    }  
	                                endif;
	                                
	                                
	                            endforeach;
                            }
                        
                        global $wpdb;
                        $tab_project = '';
                        $results = $wpdb->get_results( 'SELECT * FROM wp_posts WHERE post_type = "projet" AND post_parent = 0 AND post_status = "publish" ORDER BY RAND()', OBJECT );
                        
                        $limit = 3;
                        $i = 0;
                        
                        foreach($results as $result)
                        {
                            $categories = get_the_terms( $result->ID, 'cat_projet' );

                            // Current Loop Project Type
                            $currentLoopPostType = get_field_object('field_56c444d5f9129', $result->ID); // Project / Product Type
                            
                            
                            if(is_array($categories) && !empty($categories)){
                                foreach($categories as $category)
                                {
                                    $status_category = get_field('status', $category->taxonomy.'_'.$category->term_id);

                                    if(/*$status_category == 'En cours' && */$result->ID != $post->ID && $i < $limit && $currentLoopPostType == $currentPostType)
                                    {
                                        if(!empty($tab_project))
                                        {
                                            $tab_project .= ',';
                                        }
                                        $tab_project .= $result->ID;
                                        $i++;
                                    }
                                }
                            }
                        }
                        
                        $linkedProjects = get_field('linked_project');
                        if(!empty($linkedProjects)){
                        	$linkedProjectsTitle = 'Notre projet li&eacute;';
                        	if(strpos($linkedProjects, ',') !== false) {$linkedProjectsTitle = 'Nos projets li&eacute;s';}
                        ?>
                            <h2 class="title-other-project sub-title"><?php echo $linkedProjectsTitle; ?></h2>
                            <?php echo do_shortcode('[project ids='.$linkedProjects.']'); ?>
                        <?php }  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php bloginfo('template_directory');?>/../gefiswiss/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory');?>/../gefiswiss/js/jquery.tabbedcontent.min.js"></script>
<script type="text/javascript">
jQuery( document ).ready(function() {
    //window.$ = jQuery;
    jQuery(function() {
        
        //$(window).load(function() {
        	jQuery('div.tab-container').hide();
        	jQuery('div.tab_active div.tab-container').show();
        	jQuery('div.tab_active div.tab-container .tab-body').show();
        //});
        
        jQuery('.meo-accordion-tab h4').click(function(){            
            if(jQuery(this).parent().hasClass('tab_active'))
            {                
            	jQuery(this).parent().removeClass('tab_active');
            	jQuery(this).parent().children('div.tab-container').hide();
            }else{
            	jQuery('div.tab-container').each(function(){
            		jQuery(this).parent().removeClass('tab_active');
            		jQuery(this).hide();
               }); 
            	jQuery(this).parent().addClass('tab_active');
            	jQuery(this).parent().children('div.tab-container').show();
            }            
        });
    });
    
});
</script>
<?php get_footer(); ?>