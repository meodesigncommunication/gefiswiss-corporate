<?php
/*
 * Template Name: Tpl Product Page Subscription
 */
global $post,
$mk_options;
$page_layout = get_post_meta( $post->ID, '_layout', true );
$padding = get_post_meta( $post->ID, '_padding', true );

get_header('notitle'); ?>
<div id="theme-page" <?php echo get_schema_markup('main'); ?>>
    <div class="mk-main-wrapper-holder">
        <div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper mk-grid vc_row-fluid no-padding-margin-top">
            <div class="theme-content no-padding-margin-top" itemprop="mainContentOfPage">
                <div class="wpb_row vc_row vc_row-fluid mk-fullwidth-false attched-false vc_row-fluid">
                    <div class="vc_col-sm-12 wpb_column column_container vc_custom_1448892592966 " style="">                        
                        
                        <?php if ( have_posts() ) while ( have_posts() ) : the_post();?>
                                        <?php the_content();?>
                                        <div class="clearboth"></div>
                                        <?php wp_link_pages( 'before=<div id="mk-page-links">'.__( 'Pages:', 'mk_framework' ).'&after=</div>' ); ?>
                        <?php endwhile; ?>
                        
                    </div>
                </div>
                <?php
                $args = array(
                    'post_type' => 'produit',
                    'post_parent' => 0,
                    'order' => 'DESC'
                );
                $products = query_posts($args);
                $count = 0;
                
                // The Loop
                $html = '<div class="row">';
                foreach ($products as $product)
                {
                    if($count >= 3)
                    {
                        $html .= '</div>';
                        $html .= '<div class="row">';
                        $count = 0;
                    }
                    
                    $categories = get_the_terms($product->ID, 'cat_produit');
                    foreach ($categories as $category)
                    {
                        $status_category = get_field_object('field_56a8684a56be3', 'cat_produit_'.$category->term_id); // produit status
                        
                        if($status_category['value'] == 'Actif'){
                            $class = 'global_project status_on';
                        }else if($status_category['value'] == 'Inactif'){
                            $class = 'status_off';
                        }else{
                            $class = 'no_status';
                        }
                    } 
                    
                    $featuredImageId = get_post_thumbnail_id($product->ID);        
                    $imageUrl = wp_get_attachment_image_src($featuredImageId, 'full');
                    $categories = get_the_terms($product->ID, 'cat_produit');
                    $category = $categories[0];
                    if(!empty($imageUrl)){
                        $style = 'style="background: transparent url('.$imageUrl[0].') no-repeat; background-position: center; background-size: cover;"';
                    }else{
                        $style = 'style="background: transparent url('.get_bloginfo('url').'/wp/wp-content/uploads/2015/12/no-pics_2.jpg) no-repeat; background-position: center; background-size: cover;"';
                    }
                    
                    // $status = get_field('status', $category->taxonomy.'_'.$category->term_id);
                    $status = get_field_object('field_56a8684a56be3', 'cat_produit_'.$category->term_id); // produit status
                    
                    $printProduit = false;
                    if($status['value'] == 'En cours de souscription')
                    {
                        $type_cat = "yellow-cat";
                        $printProduit = true;
                    }else if(substr($status['value'], 0, 7) == 'Capital'){ //  entièrement souscris
                        $type_cat = "light-blue-cat";
                        $printProduit = false;
                    }else{
                        $type_cat = "blue-cat";
                        $printProduit = true;
                    }
                    
                    if($printProduit){
	                    $html .= '<div class="col-md-4 '.$class.'">';   
	                    $html .=    '<article class="bloc-project">';   
	                    $html .=        '<div '.$style.' class="vignette-img">';
	                    if(get_field( "agree_finma", $product->ID ) == 'oui'){
	                        $html .= '<div class="bandeau-finma">&nbsp;</div>';
	                    }
	                    if(get_field( "agree_chs_pp", $product->ID ) == 'oui'){
	                    	$html .= '<div class="bandeau-chs_pp">&nbsp;</div>';
	                    }
	                    $html .=            '<div class="vignette-status '.$type_cat.'">'.get_field('status', $category->taxonomy.'_'.$category->term_id).'</div>';
	                    $html .=        '</div>';
	                    $html .=        '<div class="vignette">';
	                    $html .=            '<h2>'.$product->post_title.'</h2>';               
	                    $html .=            '<p>'.substr($product->post_content,0,100).' [...]</p>';
	                    $html .=            '<a href="'.$product->guid.'" title="'.$product->post_title.'">';
	                    $html .=                '<input type="button" class="btn btn-default btn-outline-border-color" value="'.__('[:fr]En savoir plus[:en]Read more').'">';
	                    $html .=            '</a>';                    
	                    $html .=        '</div>';                     
	                    $html .=    '</article>';
	                    $html .= '</div>';
	                    
	                    $count++;
                    }
                }
                $html .= '</div>';
                echo $html;
                // Reset Query
                wp_reset_query();
                ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    window.$ = jQuery;
    /*$('#list-status').change(function(){
        var status = $(this).val();
        filterProjectsStatus(status);
    });
    function filterProjectsStatus(status)
    {
        if(status == 0){
            $('.status_1').show()
            $('.status_0').show()
            $('.status_2').show()
        }else if(status == 1){
            $('.status_1').show()
            $('.status_0').hide()
            $('.status_2').hide()
        }else if(status == 2){
            $('.status_1').hide()
            $('.status_0').hide()
            $('.status_2').show()
        }
    }*/
</script>
<?php get_footer(); ?>